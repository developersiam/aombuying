﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AOMBuying.DomainModel;
using AOMBuying.BLL;

namespace AOMBuying.BLL
{
    public interface ITransportationToWarehouseBLL
    {
        void BringUp(string baleBarcode, string recordingUser, string transportationCode);
        void BringDown(string baleBarcode);
        string Add(short crop, string truckNo, DateTime createDate, string recordingUser);
        void Print(string transportationCode);
        void Delete(string transportationCode);
        void Update(TransportationDocument transportationDocument);
        void Finish(string transportationCode);
        void UnFinish(string transportationCode);
        List<TransportationDocument> GetByCrop(short crop);
        List<TransportationDocument> GetByCreateDate(DateTime createDate);
        TransportationDocument GetSingle(string transportationCode);
    }

    public class TransportationToWarehouseBLL : ITransportationToWarehouseBLL
    {
        public void BringUp(string baleBarcode, string recordingUser, string transportationCode)
        {
            try
            {
                if (baleBarcode == null || baleBarcode == "")
                    throw new ArgumentNullException("Bale barcode is null.");

                if (recordingUser == null)
                    throw new ArgumentNullException("Recording user is null.");

                if (transportationCode == null)
                    throw new ArgumentNullException("Transportation document not found.");

                var buying = BuyingService.BuyingRepository().GetSingle(b => b.BaleBarcode == baleBarcode);
                if (buying == null)
                    throw new ArgumentNullException("Find bale data not found.");

                if (buying.BuyingWeight == null)
                    throw new ArgumentException("This bale not have a buying weight.The system cannot laod a bale to the truck.");

                if (BuyingService.TransportationDocumentRepository()
                    .GetSingle(x => x.TransportationCode == transportationCode).IsFinish == true)
                    throw new ArgumentException("This document was finished.The system cannot scan out from this document.");

                buying.TransportationCode = transportationCode;
                buying.LoadBaleToTruckDate = DateTime.Now;
                buying.LoadBaleToTruckUser = recordingUser;
                BuyingService.BuyingRepository().Update(buying);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void BringDown(string baleBarcode)
        {
            try
            {
                if (baleBarcode == null || baleBarcode == "") throw new ArgumentNullException("A bale barcode is null.");

                Buying buying = new Buying();
                buying = BuyingService.BuyingRepository().GetSingle(b => b.BaleBarcode == baleBarcode);

                if (buying == null) throw new ArgumentNullException("Find a bale barcode not found.");
                if (buying.TransportationCode == null) throw new ArgumentNullException("This bale not have scan in this document. The system cannot scan out from this document.");

                TransportationDocument transportationDocument = new TransportationDocument();
                transportationDocument = BuyingService.TransportationDocumentRepository().GetSingle(tr => tr.TransportationCode == buying.TransportationCode);

                if (transportationDocument == null) throw new ArgumentNullException("Find a Transportation document not found.");
                if (transportationDocument.IsFinish == true) throw new ArgumentException("This document was finished.The system cannot scan out from this document.");

                buying.TransportationCode = null;
                buying.LoadBaleToTruckDate = null;
                buying.LoadBaleToTruckUser = null;

                BuyingService.BuyingRepository().Update(buying);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Add(short crop, string truckNo, DateTime createDate, string recordingUser)
        {
            try
            {
                if (createDate == null) throw new ArgumentNullException("Create date is null.");
                if (recordingUser == null) throw new ArgumentNullException("Recording user is null.");

                var prefix = truckNo == null ? "WH" : "TR";

                var list = BuyingService.TransportationBLL()
                    .GetByCrop(crop)
                    .Select(x => new
                    {
                        runningNumber = Convert.ToInt32(x.TransportationCode
                        .Substring(x.TransportationCode.Length - 3, 3))
                    }).ToList();

                var max = 0;

                if (list.Count() > 0)
                    max = list.Max(x => x.runningNumber);

                var transportationCode = prefix +
                    "-" +
                    createDate.Year +
                    createDate.Month.ToString().PadLeft(2, '0') +
                    createDate.Day.ToString().PadLeft(2, '0') +
                    "-" +
                    (max + 1).ToString().PadLeft(3, '0');

                TransportationDocument transportationDocument = new TransportationDocument
                {
                    TransportationCode = transportationCode,
                    Crop = crop,
                    TruckNo = truckNo == null ? "-" : truckNo,
                    CreateDate = createDate,
                    CreateUser = recordingUser,
                    IsFinish = false,
                    FinishDate = null,
                    ModifiedDate = DateTime.Now,
                    ModifiedUser = recordingUser
                };
                BuyingService.TransportationDocumentRepository().Add(transportationDocument);
                return transportationCode;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Print(string transportationCode)
        {
            throw new NotImplementedException();
        }

        public void Delete(string transportationCode)
        {
            try
            {
                if (transportationCode == null) throw new ArgumentException("This transportation id is null.");
                TransportationDocument transportationDocument = new TransportationDocument();
                transportationDocument = GetSingle(transportationCode);

                if (transportationDocument == null) throw new ArgumentNullException("Find a transportation document not found.");
                if (transportationDocument.IsFinish == true) throw new ArgumentException("This transportation document was finished.The system cannot to delete.");

                List<Buying> buyingList = new List<Buying>();
                buyingList = BuyingService.BuyingRepository().GetList(b => b.TransportationCode == transportationCode).ToList();
                if (buyingList.Count > 0) throw new ArgumentException("This transportation document have a many bales. The system cannot to delete.");

                BuyingService.TransportationDocumentRepository().Remove(transportationDocument);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(TransportationDocument transportationDocument)
        {
            try
            {
                if (transportationDocument == null) throw new ArgumentNullException("Find a transportation document not found.");
                if (transportationDocument.TruckNo == null) throw new ArgumentNullException("A truckNo is null.");
                if (transportationDocument.ModifiedUser == null) throw new ArgumentNullException("A modified user is null.");
                if (transportationDocument.IsFinish == true) throw new ArgumentException("This transportation document was finished. The system cannot edit.");
                BuyingService.TransportationDocumentRepository().Update(transportationDocument);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Finish(string transportationCode)
        {
            try
            {

                if (transportationCode == null) throw new ArgumentNullException("A transportation id not found.");
                TransportationDocument transportationDocument = new TransportationDocument();
                transportationDocument = GetSingle(transportationCode);
                if (transportationDocument == null) throw new ArgumentNullException("Find a transportation document not found.");

                transportationDocument.IsFinish = true;
                transportationDocument.FinishDate = DateTime.Now;

                BuyingService.TransportationDocumentRepository().Update(transportationDocument);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UnFinish(string transportationCode)
        {
            try
            {
                if (transportationCode == null) throw new ArgumentNullException("A transportation id not found.");
                TransportationDocument transportationDocument = new TransportationDocument();
                transportationDocument = GetSingle(transportationCode);
                if (transportationDocument == null) throw new ArgumentNullException("Find a transportation document not found.");

                transportationDocument.IsFinish = false;
                transportationDocument.FinishDate = null;

                BuyingService.TransportationDocumentRepository().Update(transportationDocument);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<TransportationDocument> GetByCrop(short crop)
        {
            return BuyingService.TransportationDocumentRepository().GetList(t => t.Crop == crop).ToList();
        }

        public List<TransportationDocument> GetByCreateDate(DateTime createDate)
        {
            return BuyingService.TransportationDocumentRepository().GetList(t => t.CreateDate.Day == createDate.Day
            && t.CreateDate.Month == createDate.Month
            && t.CreateDate.Year == createDate.Year,
            t => t.Buyings).ToList();
        }

        public TransportationDocument GetSingle(string transportationCode)
        {
            return BuyingService.TransportationDocumentRepository()
                .GetSingle(t => t.TransportationCode == transportationCode,
                t => t.Buyings);
        }
    }
}
