﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AOMBuying.DomainModel;
using AOMBuying.BLL;

namespace AOMBuying.BLL
{
    public interface IFarmerBLL
    {
        void Add(Farmer farmer);
        void Update(Farmer farmer);
        void Delete(Farmer farmer);
        Farmer GetSingle(string farmerID);
        List<Farmer> GetAll();
    }

    public class FarmerBLL : IFarmerBLL
    {

        public void Add(Farmer farmer)
        {
            try
            {
                if (GetSingle(farmer.FarmerID) != null)
                    throw new ArgumentException("Dupplicate farmer.");

                BuyingService.FarmerRepository().Add(farmer);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(Farmer farmer)
        {
            try
            {
                if (GetSingle(farmer.FarmerID) == null)
                    throw new ArgumentNullException("Find farmer not found.");

                BuyingService.FarmerRepository().Update(farmer);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Farmer farmer)
        {
            try
            {
                if (GetSingle(farmer.FarmerID) == null)
                    throw new ArgumentNullException("Find farmer not found.");

                BuyingService.FarmerRepository().Remove(farmer);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Farmer GetSingle(string farmerID)
        {
            return BuyingService.FarmerRepository().GetSingle(fm => fm.FarmerID == farmerID);
        }

        public List<Farmer> GetAll()
        {
            return BuyingService.FarmerRepository().GetAll().ToList();
        }
    }
}
