﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AOMBuying.DomainModel;
using AOMBuying.BLL;

namespace AOMBuying.BLL
{
    public interface IBuyingDocumentBLL
    {
        BuyingDocument Add(BuyingDocument buyingDocument);
        void Delete(BuyingDocument buyingDocument);
        void Update(BuyingDocument buyingDocument);
        List<BuyingDocument> GetByCrop(short crop);
        BuyingDocument GetSingle(short crop, string farmerID, short documentNumber);
        List<BuyingDocument> GetByFarmer(short crop, string farmerID);
        List<BuyingDocument> GetByCreateDate(DateTime createDate);
        List<BuyingDocument> GetByBuyingStation(Guid buyingStationID);
        void Finish(BuyingDocument buyingDocument);
        void UnFinish(BuyingDocument buyingDocument);
    }

    public class BuyingDocumentBLL : IBuyingDocumentBLL
    {

        public BuyingDocument Add(BuyingDocument buyingDocument)
        {
            try
            {
                if (buyingDocument == null) throw new ArgumentNullException("Object value is not set.");
                if (buyingDocument.FarmerID == null) throw new ArgumentNullException("FarmerID is not set.");
                if (buyingDocument.BuyingStationID == null) throw new ArgumentNullException("BuyingStationID is not set.");
                if (buyingDocument.CreateDate == null) throw new ArgumentNullException("CreateDate is not set.");
                if (buyingDocument.CreateUser == null) throw new ArgumentNullException("CreateUser is not set.");
                if (buyingDocument.ModifiedUser == null) throw new ArgumentNullException("ModifiedUser is not set.");

                var buyingDocumentList = GetByFarmer(buyingDocument.Crop, buyingDocument.FarmerID);

                short lastDocumentNumber;

                if (buyingDocumentList.Count <= 0)
                    lastDocumentNumber = 1;
                else
                {
                    lastDocumentNumber = buyingDocumentList.Max(bd => bd.DocumentNumber);
                    lastDocumentNumber++;
                }                 

                BuyingDocument newBuyingDocument = new BuyingDocument
                {
                    Crop = buyingDocument.Crop,
                    FarmerID = buyingDocument.FarmerID,
                    DocumentNumber = lastDocumentNumber++,
                    BuyingStationID = buyingDocument.BuyingStationID,
                    CreateDate = buyingDocument.CreateDate,
                    CreateUser = buyingDocument.CreateUser,
                    IsFinish = false,
                    ModifiedDate = DateTime.Now,
                    ModifiedUser = buyingDocument.ModifiedUser
                };

                BuyingService.BuyingDocumentRepository().Add(newBuyingDocument);
                return newBuyingDocument;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(BuyingDocument buyingDocument)
        {
            try
            {
                BuyingDocument model = GetSingle(buyingDocument.Crop, buyingDocument.FarmerID, buyingDocument.DocumentNumber);

                if (model ==null) throw new ArgumentNullException("Find data not found.");
                if (model.IsFinish == true) throw new ArgumentException("This document status is finish.The system cannot delete.");
                if (model.Buyings.Count() > 0) throw new ArgumentException("This document have a many bale.The system cannot delete.");

                BuyingService.BuyingDocumentRepository().Remove(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(BuyingDocument buyingDocument)
        {
            try
            {
                if (buyingDocument == null) throw new ArgumentNullException("Object value is not set.");
                if (buyingDocument.FarmerID == null) throw new ArgumentNullException("FarmerID is not set.");
                if (buyingDocument.BuyingStationID == null) throw new ArgumentNullException("BuyingStationID is not set.");
                if (buyingDocument.CreateDate == null) throw new ArgumentNullException("CreateDate is not set.");
                if (buyingDocument.CreateUser == null) throw new ArgumentNullException("CreateUser is not set.");
                if (buyingDocument.ModifiedUser == null) throw new ArgumentNullException("ModifiedUser is not set.");

                if (GetSingle(buyingDocument.Crop, buyingDocument.FarmerID, buyingDocument.DocumentNumber) == null)
                    throw new ArgumentNullException("Find data not found.");

                BuyingService.BuyingDocumentRepository().Update(buyingDocument);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BuyingDocument> GetByCrop(short crop)
        {
            return BuyingService.BuyingDocumentRepository()
                .GetList(bd => bd.Crop == crop,
                bd => bd.Buyings,
                bd => bd.FarmerRegistration,
                bd => bd.FarmerRegistration.Farmer).ToList();
        }

        public BuyingDocument GetSingle(short crop, string farmerID, short documentNumber)
        {
            return BuyingService.BuyingDocumentRepository().GetSingle(bd => bd.Crop == crop
                && bd.FarmerID == farmerID
                && bd.DocumentNumber == documentNumber,
                bd => bd.FarmerRegistration,
                bd => bd.FarmerRegistration.Farmer,
                bd => bd.BuyingStation,
                bd => bd.Buyings,
                bd => bd.Buyings.Select(b => b.BuyingGrade1));
        }

        public List<BuyingDocument> GetByFarmer(short crop, string farmerID)
        {
            return BuyingService.BuyingDocumentRepository().GetList(bd => bd.Crop == crop
                && bd.FarmerID == farmerID,
                bd => bd.FarmerRegistration,
                bd => bd.FarmerRegistration.Farmer,
                bd => bd.BuyingStation,
                bd => bd.Buyings.Select(b => b.BuyingGrade1)).ToList();
        }

        public List<BuyingDocument> GetByCreateDate(DateTime createDate)
        {
            return BuyingService.BuyingDocumentRepository()
                .GetList(bd => bd.CreateDate.Day == createDate.Day && bd.CreateDate.Month == createDate.Month && bd.CreateDate.Year == createDate.Year,
                bd => bd.FarmerRegistration,
                bd => bd.FarmerRegistration.Farmer,
                bd => bd.BuyingStation,
                bd => bd.Buyings,
                bd => bd.Buyings.Select(b => b.BuyingGrade1)).ToList();
        }

        public List<BuyingDocument> GetByBuyingStation(Guid buyingStationID)
        {
            return BuyingService.BuyingDocumentRepository().GetList(bd => bd.BuyingStationID == buyingStationID,
                bd => bd.FarmerRegistration,
                bd => bd.FarmerRegistration.Farmer,
                bd => bd.BuyingStation).ToList();
        }

        public void Finish(BuyingDocument buyingDocument)
        {
            try
            {
                BuyingDocument model = new BuyingDocument();
                model = BuyingService.BuyingDocumentRepository()
                    .GetSingle(bd => bd.Crop == buyingDocument.Crop 
                    && bd.FarmerID == buyingDocument.FarmerID 
                    && bd.DocumentNumber == buyingDocument.DocumentNumber); //GetBuyingDocumentByID(buyingDocument.Crop, buyingDocument.FarmerID, buyingDocument.DocumentNumber);

                if (model == null)
                    throw new ArgumentNullException("Find data not found.");

                model.IsFinish = true;
                model.FinishDate = DateTime.Now;
                model.ModifiedUser = sys_config.CurrentUser;
                model.ModifiedDate = DateTime.Now;

                BuyingService.BuyingDocumentRepository().Update(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UnFinish(BuyingDocument buyingDocument)
        {
            try
            {
                BuyingDocument finishBuyingDocument = new BuyingDocument();
                finishBuyingDocument = BuyingService.BuyingDocumentRepository().GetSingle(bd => bd.Crop == buyingDocument.Crop
                    && bd.FarmerID == buyingDocument.FarmerID
                    && bd.DocumentNumber == buyingDocument.DocumentNumber); //GetBuyingDocumentByID(buyingDocument.Crop, buyingDocument.FarmerID, buyingDocument.DocumentNumber);

                if (finishBuyingDocument == null)
                    throw new ArgumentNullException("Find data not found.");

                finishBuyingDocument.IsFinish = false;
                finishBuyingDocument.FinishDate = null;
                finishBuyingDocument.ModifiedUser = buyingDocument.ModifiedUser;
                finishBuyingDocument.ModifiedDate = DateTime.Now;

                BuyingService.BuyingDocumentRepository().Update(finishBuyingDocument);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}