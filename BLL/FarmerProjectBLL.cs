﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AOMBuying.DomainModel;
using AOMBuying.BLL;

namespace AOMBuying.BLL
{
    public interface IFarmerProjectBLL
    {
        void Add(FarmerProject farmerProject);
        void Update(FarmerProject farmerProject);
        void Delete(FarmerProject farmerProject);
        FarmerProject GetSingle(short crop, string farmerID, string projectTypeID);
        List<FarmerProject> GetByFarmer(short crop, string farmerID);
        List<FarmerProject> GetByCropAndProject(short crop, string projectTypeID);
        List<FarmerProject> GetByCrop(short crop);
        List<ProjectType> GetProjectTypes();
    }

    public class FarmerProjectBLL : IFarmerProjectBLL
    {
        public void Add(FarmerProject farmerProject)
        {
            try
            {
                var _farmerProject = BuyingService.FarmerProjectRepository()
                    .GetSingle(f => f.Crop == farmerProject.Crop &&
                    f.FarmerID != farmerProject.FarmerID &&
                    f.ProjectTypeID == farmerProject.ProjectTypeID);

                if (_farmerProject != null)
                    throw new ArgumentException("The system have to already this farmer project.");

                BuyingService.FarmerProjectRepository().Add(farmerProject);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Update(FarmerProject farmerProject)
        {
            try
            {
                if (GetByFarmer(farmerProject.Crop, farmerProject.FarmerID) == null)
                    throw new ArgumentNullException("Find data not found.");

                var _farmerProject = BuyingService.FarmerProjectRepository()
                    .GetSingle(f => f.Crop == farmerProject.Crop &&
                    f.FarmerID == farmerProject.FarmerID &&
                    f.ProjectTypeID == farmerProject.ProjectTypeID);

                if (_farmerProject == null)
                    throw new ArgumentNullException("Find a farmer project not found.");

                var kgSold = BuyingService.BuyingRepository()
                    .GetList(b => b.Crop == farmerProject.Crop &&
                    b.FarmerID == farmerProject.FarmerID &&
                    b.ProjectTypeID == farmerProject.ProjectTypeID)
                    .Sum(b => b.BuyingWeight);

                if (farmerProject.Quota < kgSold)
                    throw new ArgumentException("The new quota kg less than a sold kg.You can't update this quota.");

                BuyingService.FarmerProjectRepository().Update(farmerProject);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(FarmerProject farmerProject)
        {
            try
            {
                if (GetByFarmer(farmerProject.Crop, farmerProject.FarmerID) == null)
                    throw new ArgumentNullException("Find data not found.");

                var kgSold = BuyingService.BuyingRepository()
                    .GetList(b => b.Crop == farmerProject.Crop &&
                    b.FarmerID == farmerProject.FarmerID &&
                    b.ProjectTypeID == farmerProject.ProjectTypeID)
                    .Sum(b => b.BuyingWeight);

                if (kgSold > 0)
                    throw new ArgumentException("The farmer have to sold a tobacco in this project.You can't delete quota.");

                BuyingService.FarmerProjectRepository().Remove(farmerProject);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public FarmerProject GetSingle(short crop, string farmerID, string projectTypeID)
        {
            try
            {
                return BuyingService.FarmerProjectRepository()
                    .GetSingle(fp => fp.Crop == crop &&
                    fp.FarmerID == farmerID &&
                    fp.ProjectTypeID == projectTypeID,
                    fp=>fp.FarmerRegistration,
                    fp => fp.FarmerRegistration.Farmer);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<FarmerProject> GetByFarmer(short crop, string farmerID)
        {
            return BuyingService.FarmerProjectRepository()
                .GetList(fp => fp.Crop == crop &&
                fp.FarmerID == farmerID)
                .ToList();
        }
        public List<FarmerProject> GetByCropAndProject(short crop, string projectTypeID)
        {
            return BuyingService.FarmerProjectRepository()
                .GetList(fp => fp.Crop == crop &&  fp.ProjectTypeID == projectTypeID)
                .ToList();
        }
        public List<FarmerProject> GetByCrop(short crop)
        {
            return BuyingService.FarmerProjectRepository()
                .GetList(fp => fp.Crop == crop).ToList();
        }
        public List<ProjectType> GetProjectTypes()
        {
            return BuyingService.ProjectTypeRepository().GetAll().ToList();
        }
    }
}
