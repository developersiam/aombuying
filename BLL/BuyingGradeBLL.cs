﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AOMBuying.DomainModel;
using AOMBuying.BLL;

namespace AOMBuying.BLL
{
    public interface IBuyingGradeBLL
    {
        void AddBuyingGrade(BuyingGrade buyingGrade);
        void UpdateBuyingGrade(BuyingGrade buyingGrade);
        void DeleteBuyingGrade(BuyingGrade buyingGrade);
        List<BuyingGrade> GetBuyingGradeByCrop(short crop);
        BuyingGrade GetBuyingGradeByID(short crop, string buyingGrade);
    }

    public class BuyingGradeBLL : IBuyingGradeBLL
    {

        public void AddBuyingGrade(BuyingGrade buyingGrade)
        {
            try
            {
                if (buyingGrade == null)
                    throw new ArgumentNullException("Object reference is not set.");

                if (buyingGrade.BuyingGrade1 == null || buyingGrade.BuyingGrade1 == "")
                    throw new ArgumentNullException("Buying grade reference is not set.");

                if (buyingGrade.ModifiedUser == null || buyingGrade.ModifiedUser == "")
                    throw new ArgumentNullException("Modified user reference is not set.");

                if (buyingGrade.ProjectTypeID == null || buyingGrade.ProjectTypeID == "")
                    throw new ArgumentNullException("ProjectTypeID reference is not set.");


                if (GetBuyingGradeByID(buyingGrade.BuyingGradeCrop, buyingGrade.BuyingGrade1) != null)
                    throw new ArgumentNullException("Dupplicate data!");


                BuyingService.BuyingGradeRepository().Add(buyingGrade);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateBuyingGrade(BuyingGrade buyingGrade)
        {
            try
            {
                if (buyingGrade == null)
                    throw new ArgumentNullException("Object reference is not set.");

                if (buyingGrade.BuyingGrade1 == null || buyingGrade.BuyingGrade1 == "")
                    throw new ArgumentNullException("Buying grade reference is not set.");

                if (buyingGrade.ModifiedUser == null || buyingGrade.ModifiedUser == "")
                    throw new ArgumentNullException("Modified user reference is not set.");

                if (buyingGrade.ProjectTypeID == null || buyingGrade.ProjectTypeID == "")
                    throw new ArgumentNullException("ProjectTypeID reference is not set.");


                BuyingGrade currentBuyingGrade = new BuyingGrade();
                currentBuyingGrade = GetBuyingGradeByID(buyingGrade.BuyingGradeCrop, buyingGrade.BuyingGrade1);


                if (currentBuyingGrade == null)
                    throw new ArgumentNullException("Find not found data!");

                currentBuyingGrade.ProjectTypeID = buyingGrade.ProjectTypeID;
                currentBuyingGrade.Quality = buyingGrade.Quality;
                currentBuyingGrade.UnitPrice = buyingGrade.UnitPrice;
                currentBuyingGrade.ModifiedUser = buyingGrade.ModifiedUser;
                currentBuyingGrade.ModifiedDate = DateTime.Now;


                BuyingService.BuyingGradeRepository().Update(currentBuyingGrade);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteBuyingGrade(BuyingGrade buyingGrade)
        {
            try
            {
                if (buyingGrade == null)
                    throw new ArgumentNullException("Object reference is not set.");


                BuyingGrade currentBuyingGrade = new BuyingGrade();
                currentBuyingGrade = GetBuyingGradeByID(buyingGrade.BuyingGradeCrop, buyingGrade.BuyingGrade1);


                if (currentBuyingGrade == null)
                    throw new ArgumentNullException("Find not found data!");
                

                BuyingService.BuyingGradeRepository().Remove(currentBuyingGrade);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BuyingGrade> GetBuyingGradeByCrop(short crop)
        {
            return BuyingService.BuyingGradeRepository().GetList(bg => bg.BuyingGradeCrop == crop).ToList();
        }

        public BuyingGrade GetBuyingGradeByID(short crop, string buyingGrade)
        {
            return BuyingService.BuyingGradeRepository().GetSingle(bg => bg.BuyingGradeCrop == crop && bg.BuyingGrade1 == buyingGrade);
        }
    }
}
