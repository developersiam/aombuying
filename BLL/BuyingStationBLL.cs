﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AOMBuying.DomainModel;

namespace AOMBuying.BLL
{
    public interface IBuyingStationBLL
    {
        void Delete(BuyingStation model);
        void Add(BuyingStation model);
        void Edit(BuyingStation model);
        List<BuyingStation> GetAll();
    }

    public class BuyingStationBLL : IBuyingStationBLL
    {
        public void Add(BuyingStation model)
        {
            try
            {
                if (model == null) throw new ArgumentNullException("A new buying station is null.");
                if (model.BuyingStationID == null) throw new ArgumentNullException("A BuyingStationID is null.");
                if (model.BuyingStationName == null) throw new ArgumentNullException("A BuyingStationName is null.");

                BuyingStation buyingStation = new BuyingStation();
                buyingStation = BuyingService.BuyingStationRepository().GetSingle(b => b.BuyingStationName == model.BuyingStationName);
                if (buyingStation != null) throw new ArgumentException("The system have to a station already.");

                model.ModifiedDate = DateTime.Now;
                model.ModifiedUser = sys_config.CurrentUser;
                BuyingService.BuyingStationRepository().Add(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(BuyingStation model)
        {
            try
            {
                if (model == null) throw new ArgumentNullException("A new buying station is null.");
                if (model.BuyingStationID == null) throw new ArgumentNullException("A BuyingStationID is null.");
                if (model.BuyingStationName == null) throw new ArgumentNullException("A BuyingStationName is null.");

                BuyingStation buyingStation = new BuyingStation();
                buyingStation = BuyingService.BuyingStationRepository().GetSingle(b => b.BuyingStationID == model.BuyingStationID);
                if (buyingStation == null) throw new ArgumentNullException("Find a data not found!");

                model.ModifiedDate = DateTime.Now;
                model.ModifiedUser = sys_config.CurrentUser;
                BuyingService.BuyingStationRepository().Remove(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(BuyingStation model)
        {
            try
            {
                if (model == null) throw new ArgumentNullException("A new buying station is null.");
                if (model.BuyingStationID == null) throw new ArgumentNullException("A BuyingStationID is null.");
                if (model.BuyingStationName == null) throw new ArgumentNullException("A BuyingStationName is null.");

                BuyingStation buyingStation = new BuyingStation();
                buyingStation = BuyingService.BuyingStationRepository().GetSingle(b => b.BuyingStationID == model.BuyingStationID);
                if (buyingStation == null) throw new ArgumentNullException("Find a data not found!");

                BuyingService.BuyingStationRepository().Update(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BuyingStation> GetAll()
        {
            return BuyingService.BuyingStationRepository().GetAll().OrderBy(b => b.BuyingStationName).ToList();
        }
    }
}
