﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AOMBuying.DomainModel;
using AOMBuying.BLL;

namespace AOMBuying.BLL
{
    public interface IFarmerRegistrationBLL
    {
        void AddFarmerRegistration(FarmerRegistration farmerRegistration);
        void DeleteFarmerRegistration(FarmerRegistration farmerRegistration);
        List<FarmerRegistration> GetFarmerRegistrationByCrop(short crop);
        List<FarmerRegistration> GetFarmerRegistrationByFarmerID(string FarmerID);
        FarmerRegistration GetFarmerRegistrationByID(short crop, string FarmerID);
    }

    public class FarmerRegistrationBLL : IFarmerRegistrationBLL
    {
        public void AddFarmerRegistration(FarmerRegistration farmerRegistration)
        {
            try
            {
                if (farmerRegistration == null)
                    throw new ArgumentNullException("Object value is not set.");

                if (farmerRegistration.FarmerID == null)
                    throw new ArgumentNullException("FarmerID is not set.");

                if (farmerRegistration.ModifiedUser == null)
                    throw new ArgumentNullException("ModifiedUser is not set.");

                if (farmerRegistration.RegistrationDate == null)
                    throw new ArgumentNullException("RegistrationDate is not set.");


                if (GetFarmerRegistrationByID(farmerRegistration.Crop, farmerRegistration.FarmerID) != null)
                    throw new ArgumentException("Dupplicate data.");


                BuyingService.FarmerRegistrationRepository().Add(farmerRegistration);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteFarmerRegistration(FarmerRegistration farmerRegistration)
        {
            try
            {
                FarmerRegistration deleteFarmerRegistration = new FarmerRegistration();
                farmerRegistration = GetFarmerRegistrationByID(farmerRegistration.Crop, farmerRegistration.FarmerID);

                if (deleteFarmerRegistration == null)
                    throw new ArgumentNullException("Find data not found.");

                BuyingService.FarmerRegistrationRepository().Remove(deleteFarmerRegistration);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<FarmerRegistration> GetFarmerRegistrationByCrop(short crop)
        {
            return BuyingService.FarmerRegistrationRepository().GetList(fr => fr.Crop == crop,
                fr => fr.FarmerProjects,
                fr => fr.Farmer).ToList();
        }

        public List<FarmerRegistration> GetFarmerRegistrationByFarmerID(string FarmerID)
        {
            return BuyingService.FarmerRegistrationRepository().GetList(fr => fr.FarmerID == FarmerID,
                fr => fr.FarmerProjects,
                fr => fr.Farmer).ToList();
        }

        public FarmerRegistration GetFarmerRegistrationByID(short crop, string FarmerID)
        {
            return BuyingService.FarmerRegistrationRepository().GetSingle(fr => fr.Crop == crop
                && fr.FarmerID == FarmerID,
                fr => fr.FarmerProjects,
                fr => fr.Farmer);
        }
    }
}
