﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AOMBuying.DomainModel;

namespace AOMBuying.BLL
{
    public interface ICropBLL
    {
        Crop GetDefaultCrop();
        List<Crop> GetAllCrops();
    }

    public class CropBLL : ICropBLL
    {
        public List<Crop> GetAllCrops()
        {
            return BuyingService.CropRepository().GetAll().ToList();
        }

        public Crop GetDefaultCrop()
        {
            return BuyingService.CropRepository().GetSingle(c => c.DefaultStatus == true);
        }
    }
}
