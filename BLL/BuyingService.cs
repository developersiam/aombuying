﻿using AOMBuying.BLL;
using AOMBuying.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOMBuying.BLL
{
    public static class BuyingService
    {
        public static BuyingRepository BuyingRepository() {
            BuyingRepository repo = new BuyingRepository();
            return repo;
        }

        public static BuyingDocumentRepository BuyingDocumentRepository()
        {
            BuyingDocumentRepository repo = new BuyingDocumentRepository();
            return repo;
        }

        public static BuyingGradeRepository BuyingGradeRepository()
        {
            BuyingGradeRepository repo = new BuyingGradeRepository();
            return repo;
        }

        public static RejectTypeRepository RejectTypeRepository()
        {
            RejectTypeRepository repo = new RejectTypeRepository();
            return repo;
        }

        public static ProjectTypeRepository ProjectTypeRepository()
        {
            ProjectTypeRepository repo = new ProjectTypeRepository();
            return repo;
        }

        public static FarmerProjectRepository FarmerProjectRepository()
        {
            FarmerProjectRepository repo = new FarmerProjectRepository();
            return repo;
        }

        public static CropRepository CropRepository()
        {
            CropRepository repo = new CropRepository();
            return repo;
        }

        public static TransportationDocumentRepository TransportationDocumentRepository()
        {
            TransportationDocumentRepository repo = new TransportationDocumentRepository();
            return repo;
        }

        public static BuyingStationRepository BuyingStationRepository()
        {
            BuyingStationRepository repo = new BuyingStationRepository();
            return repo;
        }

        public static FarmerRepository FarmerRepository()
        {
            FarmerRepository repo = new FarmerRepository();
            return repo;
        }
        
        public static FarmerRegistrationRepository FarmerRegistrationRepository()
        {
            FarmerRegistrationRepository repo = new FarmerRegistrationRepository();
            return repo;
        }



        public static ICropBLL CropBLL()
        {
            ICropBLL bll = new CropBLL();
            return bll;
        }

        public static IBuyingBLL BuyingBLL()
        {
            IBuyingBLL bll = new BuyingBLL();
            return bll;
        }

        public static IBuyingDocumentBLL BuyingDocumentBLL()
        {
            IBuyingDocumentBLL bll = new BuyingDocumentBLL();
            return bll;
        }

        public static IBuyingGradeBLL BuyingGradeBLL()
        {
            IBuyingGradeBLL bll = new BuyingGradeBLL();
            return bll;
        }

        public static IBuyingStationBLL BuyingStationBLL()
        {
            IBuyingStationBLL bll = new BuyingStationBLL();
            return bll;
        }

        public static IFarmerBLL FarmerBLL()
        {
            IFarmerBLL bll = new FarmerBLL();
            return bll;
        }

        public static IFarmerProjectBLL FarmerProjectBLL()
        {
            IFarmerProjectBLL bll = new FarmerProjectBLL();
            return bll;
        }

        public static IFarmerRegistrationBLL FarmerRegistrationBLL()
        {
            IFarmerRegistrationBLL bll = new FarmerRegistrationBLL();
            return bll;
        }

        public static ITransportationToWarehouseBLL TransportationBLL()
        {
            ITransportationToWarehouseBLL bll = new TransportationToWarehouseBLL();
            return bll;
        }

        public static IRejectTypeBLL RejectTypeBLL()
        {
            IRejectTypeBLL bll = new RejectTypeBLL();
            return bll;
        }
    }
}
