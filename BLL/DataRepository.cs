﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AOMBuying.DAL;
using AOMBuying.DomainModel;

namespace AOMBuying.BLL
{
    public interface IBuyingRepository : IGenericDataRepository<Buying> { }
    public interface IBuyingDocumentRepository : IGenericDataRepository<BuyingDocument> { }
    public interface IBuyingGradeRepository : IGenericDataRepository<BuyingGrade> { }
    public interface IBuyingStationRepository : IGenericDataRepository<BuyingStation> { }
    public interface ICropRepository : IGenericDataRepository<Crop> { }
    public interface IFarmerRepository : IGenericDataRepository<Farmer> { }
    public interface IFarmerProjectRepository : IGenericDataRepository<FarmerProject> { }
    public interface IFarmerRegistrationRepository : IGenericDataRepository<FarmerRegistration> { }
    public interface IProjectTypeRepository : IGenericDataRepository<ProjectType> { }
    public interface IRejectTypeRepository : IGenericDataRepository<RejectType> { }
    public interface ITransportationDocumentRepository : IGenericDataRepository<TransportationDocument> { }
    

    public class BuyingRepository : GenericDataRepository<Buying>, IBuyingRepository { }
    public class BuyingDocumentRepository : GenericDataRepository<BuyingDocument>, IBuyingDocumentRepository { }
    public class BuyingGradeRepository : GenericDataRepository<BuyingGrade>, IBuyingGradeRepository { }
    public class BuyingStationRepository : GenericDataRepository<BuyingStation>, IBuyingStationRepository { }
    public class CropRepository : GenericDataRepository<Crop>, ICropRepository { }
    public class FarmerRepository : GenericDataRepository<Farmer>, IFarmerRepository { }
    public class FarmerProjectRepository : GenericDataRepository<FarmerProject>, IFarmerProjectRepository { }
    public class FarmerRegistrationRepository : GenericDataRepository<FarmerRegistration>, IFarmerRegistrationRepository { }
    public class ProjectTypeRepository : GenericDataRepository<ProjectType>, IProjectTypeRepository { }
    public class RejectTypeRepository : GenericDataRepository<RejectType>, IRejectTypeRepository { }
    public class TransportationDocumentRepository : GenericDataRepository<TransportationDocument>, ITransportationDocumentRepository { }
}
