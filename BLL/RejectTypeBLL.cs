﻿using AOMBuying.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOMBuying.BLL
{
    public interface IRejectTypeBLL
    {
        List<RejectType> GetAll();

        RejectType GetSingle(Guid rejectTypeID);
    }

    public class RejectTypeBLL : IRejectTypeBLL
    {
        public List<RejectType> GetAll()
        {
            return BuyingService.RejectTypeRepository().GetAll().ToList();
        }

        public RejectType GetSingle(Guid rejectTypeID)
        {
            return BuyingService.RejectTypeRepository().GetSingle(x => x.RejectTypeID == rejectTypeID);
        }
    }
}
