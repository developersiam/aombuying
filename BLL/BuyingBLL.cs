﻿using AOMBuying.DomainModel;
using AOMBuying.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Printing;

namespace AOMBuying.BLL
{
    public interface IBuyingBLL
    {
        string Add(BuyingDocument buyingDocument, string projectTypeID, string recordingUser);
        void PrintBaleBarcodeSticker(string baleBarcode);
        void Delete(string baleBarcode);
        void RejectBale(string baleBarcode, Guid rejectTypeID);
        void CancelRejectBale(string baleBarcode);
        void CaptureBuyingInformation(string baleBarcode,
            string buyingGrade, decimal weight, string recordingUser,
            DateTime recordingDate, BuyingDocument buyingDocument);
        void CaptureBuyingInformationWithTransportation(string baleBarcode,
            string buyingGrade, decimal weight, string recordingUser, string transportationCode,
            DateTime recordingDate, BuyingDocument buyingDocument);
        Buying GetByBaleBarcode(string baleBarcode);
        Buying GetByBaleNumber(int baleNumber);
        List<Buying> GetByDocument(BuyingDocument buyingDocument);
        List<Buying> GetByCrop(short crop);
        List<Buying> GetByBuyingDate(DateTime buyingDate);
        List<Buying> GetByCropAndProjectType(short crop, string projectTypeID);
        List<RejectType> GetRejectTypes();
        List<Buying> GetByFarmer(short crop, string farmerID);
        List<Buying> GetByTransportationDocument(string transportationCode);
    }

    public class BuyingBLL : IBuyingBLL
    {
        public string Add(BuyingDocument buyingDocument, string projectTypeID, string recordingUser)
        {
            try
            {
                //Check farmer have quota in this project type.
                FarmerProject farmerProject = new FarmerProject();
                farmerProject = BuyingService.FarmerProjectRepository().GetSingle(fp => fp.ProjectTypeID == projectTypeID
                    && fp.FarmerID == buyingDocument.FarmerID
                    && fp.Crop == buyingDocument.Crop);

                if (farmerProject == null)
                    throw new ArgumentNullException("This farmer not have a quota in " + projectTypeID + " project.");

                //Generate new bale barcode. *******************************
                //CY2018-FCV-V336-1-1
                var buyingList = GetByDocument(buyingDocument);
                //Check over quota for this project type.
                if (buyingList.Sum(b => b.BuyingWeight) > farmerProject.Quota)
                    throw new ArgumentException("Over quota! Farmer sold "
                        + buyingList.Sum(b => b.BuyingWeight)
                        + " Kg. and farmer quota is "
                        + farmerProject.Quota + " Kg.");

                short lastBaleBarcode = 0;

                if (buyingList.Count <= 0)
                    lastBaleBarcode = 1;
                else
                {
                    lastBaleBarcode = Convert.ToInt16(buyingList.Count);
                    lastBaleBarcode++;
                }

                string newBarcode = "CY" + buyingDocument.Crop + "-"
                    + projectTypeID + "-" + buyingDocument.FarmerID + "-"
                    + buyingDocument.DocumentNumber + "-" + lastBaleBarcode;

                //Craate new buying information ******************************
                Buying buying = new Buying
                {
                    BaleBarcode = newBarcode,
                    Crop = buyingDocument.Crop,
                    FarmerID = buyingDocument.FarmerID,
                    DocumentNumber = buyingDocument.DocumentNumber,
                    ProjectTypeID = projectTypeID,
                    RegisterBarcodeUser = recordingUser,
                    RegisterBarcodeDate = DateTime.Now
                };

                BuyingService.BuyingRepository().Add(buying);
                return buying.BaleBarcode;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void PrintBaleBarcodeSticker(string baleBarcode)
        {
            try
            {
                var buying = BuyingService.BuyingBLL().GetByBaleBarcode(baleBarcode);
                if (buying == null) throw new ArgumentException("Find a buying data not found!");

                var farmer = BuyingService.FarmerBLL().GetSingle(buying.FarmerID);
                if (farmer == null) throw new ArgumentException("Find a farmer data not found!");

                PrinterSettings setting = new PrinterSettings();

                TSCPrinter.openport(setting.PrinterName);
                TSCPrinter.setup("103", "76", "2.0", "1", "0", "0", "0");
                TSCPrinter.sendcommand("GAP  3 mm,0");
                TSCPrinter.sendcommand("DIRECTION 1");
                TSCPrinter.clearbuffer();

                //TSCPrinter.sendcommand("BAR 40,40,4,528");          //เส้นตั้ง หน้าสุด
                //TSCPrinter.sendcommand("BAR 776,40,4,528");          //เส้นตั้ง หลังสุด
                //TSCPrinter.sendcommand("BAR 40,40,736,4");          //เส้นนอน บนสุด
                //TSCPrinter.sendcommand("BAR 40,568,736,4");          //เส้นนอน ล่างสุด

                TSCPrinter.barcode("40", "20", "128", "120", "0", "0", "2", "2", buying.BaleBarcode);
                TSCPrinter.windowsfont(40, 140, 30, 0, 0, 0, "arial", buying.BaleBarcode);
                TSCPrinter.barcode("660", "552", "128", "120", "0", "270", "2", "2", buying.BaleBarcode);
                //TSCPrinter.windowsfont(660, 520, 30, 90, 0, 0, "arial", buying.BaleBarcode);
                TSCPrinter.windowsfont(80, 200, 80, 0, 0, 0, "arial", "ID : " + buying.FarmerID);
                TSCPrinter.windowsfont(80, 280, 40, 0, 0, 0, "arial", "Name : " + farmer.FarmerName);
                TSCPrinter.windowsfont(80, 360, 40, 0, 0, 0, "arial", "CY : " + buying.Crop);
                TSCPrinter.windowsfont(80, 400, 40, 0, 0, 0, "arial", "Type : " + buying.ProjectTypeID);
                TSCPrinter.windowsfont(80, 440, 40, 0, 0, 0, "arial", "Bale No : " + buying.BaleNumber);
                TSCPrinter.windowsfont(80, 480, 40, 0, 0, 0, "arial", "Regis Date : " + buying.RegisterBarcodeDate.ToShortDateString()
                    + " " + buying.RegisterBarcodeDate.ToLongTimeString());

                TSCPrinter.printlabel("1", "1");
                TSCPrinter.closeport();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(string baleBarcode)
        {
            try
            {
                if (baleBarcode == null || baleBarcode == "")
                    throw new ArgumentNullException("The bale barcode is null.");

                var buying = BuyingService.BuyingRepository()
                    .GetSingle(b => b.BaleBarcode == baleBarcode);
                if (buying == null)
                    throw new ArgumentNullException("Find data not found.");

                if (buying.BuyingGrade != null)
                    throw new ArgumentException("This barcode have a buying grade.The system not allow to delete.");

                if (buying.RejectTypeID != null)
                    throw new ArgumentException("This barcode was reject.The system not allow to delete.");

                if (buying.BuyingWeight != null)
                    throw new ArgumentException("This barcode have a buying weight.The system not allow to delete.");

                if (buying.TransportationCode != null)
                    throw new ArgumentException("This barcode have loading to truck.The system not allow to delete.");

                var buyingDocument = BuyingService.BuyingDocumentRepository()
                    .GetSingle(bd => bd.Crop == buying.Crop
                    && bd.FarmerID == buying.FarmerID
                    && bd.DocumentNumber == buying.DocumentNumber);

                if (buyingDocument == null)
                    throw new ArgumentNullException("Find the buying document data not found.");
                if (buyingDocument.IsFinish == true)
                    throw new ArgumentException("The buying document set to finish status. The system not allow to delete.");

                BuyingService.BuyingRepository().Remove(buying);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void RejectBale(string baleBarcode, Guid rejectTypeID)
        {
            try
            {
                if (baleBarcode == null || baleBarcode == "")
                    throw new ArgumentNullException("The bale barcode is null.");

                if (rejectTypeID == null)
                    throw new ArgumentNullException("A reject type ID is null.");

                var buying = BuyingService.BuyingRepository()
                     .GetSingle(b => b.BaleBarcode == baleBarcode);

                if (buying == null)
                    throw new ArgumentNullException("Find the bale data not found.");
                if (buying.LoadBaleToTruckDate != null)
                    throw new ArgumentException("This bale have a recode about transportation already. The system not allow to reject.");

                var buyingDocument = BuyingService.BuyingDocumentRepository()
                    .GetSingle(bd => bd.Crop == buying.Crop
                    && bd.FarmerID == buying.FarmerID
                    && bd.DocumentNumber == buying.DocumentNumber);

                if (buyingDocument == null)
                    throw new ArgumentNullException("Find the buying document data not found.");
                if (buyingDocument.IsFinish == true)
                    throw new ArgumentException("The buying document set to finish status. The system not allow to reject.");

                buying.RejectTypeID = rejectTypeID;
                buying.BuyingGrade = null;
                buying.BuyingWeight = null;

                BuyingService.BuyingRepository().Update(buying);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void CancelRejectBale(string baleBarcode)
        {
            try
            {
                if (baleBarcode == null || baleBarcode == "")
                    throw new ArgumentNullException("Bale barcode is null.");

                var buying = BuyingService.BuyingRepository()
                    .GetSingle(b => b.BaleBarcode == baleBarcode);

                if (buying == null)
                    throw new ArgumentNullException("Find a bale data not found.");

                var buyingDocument = BuyingService.BuyingDocumentRepository()
                    .GetSingle(bd => bd.Crop == buying.Crop
                    && bd.FarmerID == buying.FarmerID
                    && bd.DocumentNumber == buying.DocumentNumber);

                if (buyingDocument == null)
                    throw new ArgumentNullException("Find buying document data not found.");
                if (buyingDocument.IsFinish == true)
                    throw new ArgumentException("The buying document set to finish status. The system not allow to reject.");

                buying.RejectTypeID = null;
                BuyingService.BuyingRepository().Update(buying);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void CaptureBuyingInformationWithTransportation(string baleBarcode,
            string buyingGrade, decimal weight, string recordingUser, string transportationCode,
            DateTime recordingDate, BuyingDocument buyingDocument)
        {
            try
            {
                if (weight <= 0)
                    throw new ArgumentNullException("The buying weight cannot to 0 kg.");

                var buying = GetByBaleBarcode(baleBarcode);
                if (buying == null)
                    throw new ArgumentNullException("Find the bale not found.");

                if (buyingDocument.IsFinish == true)
                    throw new ArgumentException("The buying document was finished.The system cannot save.");

                if (buying.TransportationDocument != null)
                    if (buying.TransportationDocument.IsFinish == true)
                        throw new ArgumentException("This transportation document was to finish.The system cannot to save.");

                buying.BuyingGrade1 = null;
                buying.BuyingGradeCrop = buyingDocument.Crop;
                buying.BuyingGrade = buyingGrade;
                buying.RejectTypeID = null;
                buying.BuyingGradeRecordUser = recordingUser;
                buying.BuyingGradeRecordDate = recordingDate;
                buying.BuyingWeight = weight;
                buying.BuyingWeightRecordDate = recordingDate;
                buying.BuyingWeightRecordUser = recordingUser;
                buying.TransportationCode = transportationCode;
                buying.LoadBaleToTruckDate = DateTime.Now;
                buying.LoadBaleToTruckUser = sys_config.CurrentUser;

                BuyingService.BuyingRepository().Update(buying);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void CaptureBuyingInformation(string baleBarcode,
            string buyingGrade, decimal weight, string recordingUser,
            DateTime recordingDate, BuyingDocument buyingDocument)
        {
            try
            {
                if (weight <= 0)
                    throw new ArgumentNullException("The buying weight cannot to 0 kg.");

                Buying buying = new Buying();
                buying = GetByBaleBarcode(baleBarcode);

                //buying = BuyingService._buyingRepository().GetSingle(b => b.BaleBarcode == baleBarcode);
                if (buying == null) throw new ArgumentNullException("Find the bale not found.");
                //if (buying.LoadBaleToTruckDate != null) throw new ArgumentException("This bale have to capture a transportation info already.The system cannot save.");
                if (buyingDocument.IsFinish == true)
                    throw new ArgumentException("The buying document was finished.The system cannot save.");

                buying.BuyingGrade1 = null;
                buying.BuyingGradeCrop = buyingDocument.Crop;
                buying.BuyingGrade = buyingGrade;
                buying.RejectType = null;
                buying.RejectTypeID = null;
                buying.BuyingGradeRecordUser = recordingUser;
                buying.BuyingGradeRecordDate = recordingDate;
                buying.BuyingWeight = weight;
                buying.BuyingWeightRecordDate = recordingDate;
                buying.BuyingWeightRecordUser = recordingUser;

                BuyingService.BuyingRepository().Update(buying);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Buying GetByBaleBarcode(string baleBarcode)
        {
            return BuyingService.BuyingRepository()
                .GetSingle(b => b.BaleBarcode == baleBarcode,
                b => b.RejectType,
                b => b.BuyingGrade1,
                b => b.BuyingDocument,
                b => b.TransportationDocument);
        }
        public Buying GetByBaleNumber(int baleNumber)
        {
            return BuyingService.BuyingRepository()
                .GetSingle(b => b.BaleNumber == baleNumber,
                b => b.RejectType,
                b => b.BuyingGrade1,
                b => b.BuyingDocument,
                b => b.TransportationDocument);
        }
        public List<Buying> GetByDocument(BuyingDocument buyingDocument)
        {
            return BuyingService.BuyingRepository()
                .GetList(b => b.Crop == buyingDocument.Crop
                && b.FarmerID == buyingDocument.FarmerID
                && b.DocumentNumber == buyingDocument.DocumentNumber,
                b => b.RejectType,
                b => b.BuyingGrade1,
                b => b.TransportationDocument).ToList();
        }
        public List<Buying> GetByCrop(short crop)
        {
            return BuyingService.BuyingRepository()
                .GetList(b => b.Crop == crop,
                b => b.RejectType,
                b => b.BuyingGrade1,
                b => b.TransportationDocument).ToList();
        }
        public List<Buying> GetByBuyingDate(DateTime buyingDate)
        {
            var list = BuyingService.BuyingRepository()
                .GetList(b => b.RegisterBarcodeDate.Day == buyingDate.Day
                && b.RegisterBarcodeDate.Month == buyingDate.Month
                && b.RegisterBarcodeDate.Year == buyingDate.Year
                //b.RegisterBarcodeDate.Day
                //+ b.RegisterBarcodeDate.Month
                //+ b.RegisterBarcodeDate.Year == buyingDate.Day
                //+ buyingDate.Month + buyingDate.Year
                ,
                b => b.RejectType,
                b => b.BuyingGrade1,
                b => b.TransportationDocument).ToList();

            //Filter bales to sold.
            return list; //.Where(l => l.BuyingWeight != null).ToList();

            //Filter bales not loading to truck.
            //return list2.Where(l => l.TransportationCode == null).ToList();
        }
        public List<Buying> GetByCropAndProjectType(short crop, string projectTypeID)
        {
            return BuyingService.BuyingRepository()
                .GetList(b => b.Crop == crop &&
                b.ProjectTypeID == projectTypeID,
                b => b.RejectType,
                b => b.BuyingGrade1,
                b => b.TransportationDocument).ToList();
        }
        public List<RejectType> GetRejectTypes()
        {
            return BuyingService.RejectTypeRepository().GetAll().ToList();
        }
        public List<Buying> GetByFarmer(short crop, string farmerID)
        {
            return BuyingService.BuyingRepository()
                .GetList(b => b.Crop == crop
                && b.FarmerID == farmerID,
                b => b.RejectType,
                b => b.BuyingGrade1,
                b => b.TransportationDocument).ToList();
        }

        public List<Buying> GetByTransportationDocument(string transportationCode)
        {
            return BuyingService.BuyingRepository()
                .GetList(b => b.TransportationCode == transportationCode,
                b => b.RejectType,
                b => b.BuyingGrade1,
                b => b.TransportationDocument).ToList();
        }
    }
}
