//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AOMBuying.DomainModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Buying
    {
        public string BaleBarcode { get; set; }
        public int BaleNumber { get; set; }
        public short Crop { get; set; }
        public string FarmerID { get; set; }
        public short DocumentNumber { get; set; }
        public string ProjectTypeID { get; set; }
        public System.DateTime RegisterBarcodeDate { get; set; }
        public string RegisterBarcodeUser { get; set; }
        public Nullable<System.Guid> RejectTypeID { get; set; }
        public Nullable<short> BuyingGradeCrop { get; set; }
        public string BuyingGrade { get; set; }
        public Nullable<System.DateTime> BuyingGradeRecordDate { get; set; }
        public string BuyingGradeRecordUser { get; set; }
        public Nullable<decimal> BuyingWeight { get; set; }
        public Nullable<System.DateTime> BuyingWeightRecordDate { get; set; }
        public string BuyingWeightRecordUser { get; set; }
        public Nullable<System.DateTime> LoadBaleToTruckDate { get; set; }
        public string LoadBaleToTruckUser { get; set; }
        public string TransportationCode { get; set; }
    
        public virtual BuyingDocument BuyingDocument { get; set; }
        public virtual BuyingGrade BuyingGrade1 { get; set; }
        public virtual ProjectType ProjectType { get; set; }
        public virtual RejectType RejectType { get; set; }
        public virtual TransportationDocument TransportationDocument { get; set; }
    }
}
