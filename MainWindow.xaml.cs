﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AOMBuying
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void RPT001Menu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new Report.RPT001Page());
        }

        private void RPT002Menu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new Report.RPT002Page());
        }

        private void RPT003Menu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new Report.RPT003Page());
        }

        private void RPT004Menu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new Report.RPT004Page());
        }

        private void RPT005Menu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new Report.RPT005Page());
        }

        private void RPT006Menu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new Report.RPT006Page());
        }

        private void RPT007Menu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new Report.RPT007Page());
        }

        private void RPT008Menu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new Report.RPT008Page());
        }

        private void UIElement_OnPreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void MenuPopupButton_OnClick(object sender, RoutedEventArgs e)
        {

        }

        private void OnCopy(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void HomeMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.Menus.Home());
        }

        private void FarmerQuotaMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.Setups.SetupFarmerQuotaPage());
        }

        private void BuyingStationMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.Setups.SetupBuyingStationPage());
        }

        private void BuyingGradeMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.Setups.SetupBuyingGradePage());
        }
    }
}
