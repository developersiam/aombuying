﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOMBuying.DomainModel;

namespace AOMBuying.ViewModel
{
    public class vm_FarmerCropQuota : FarmerProject
    {
        public double TotalBale { get; set; }
        public double KgSold { get; set; }
        public double KgBalance { get; set; }
        public double TotalPrice { get; set; }
        public FarmerProject FarmerProject { get; set; }
    }
}
