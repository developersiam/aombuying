﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOMBuying.DomainModel;

namespace AOMBuying.ViewModel
{
    public class vm_BuyingDocument : BuyingDocument
    {
        public Farmer Farmer { get; set; }
        public int TotalBale { get; set; }
        public int TotalBuying { get; set; }
        public int TotalReject { get; set; }
        public int TotalLoadTruck { get; set; }
        public decimal TotalWeight { get; set; }
        public decimal TotalPrice { get; set; }
        public string BuyingDocumentCode { get; set; }
    }
}
