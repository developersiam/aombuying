﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOMBuying.DomainModel;

namespace AOMBuying.ViewModel
{
    public class vm_Buying : Buying
    {
        public string BuyingDocumentCode { get; set; }
        public decimal UnitPrice { get; set; }
        public string Quality { get; set; }
        public decimal Price { get; set; }
        public string TruckNo { get; set; }
        public Nullable<System.DateTime> CreateTransportationDate { get; set; }
        public string RejectTypeName { get; set; }
    }
}
