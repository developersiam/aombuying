﻿using AOMBuying.BLL;
using AOMBuying.DomainModel;
using AOMBuying.Utility;
using AOMBuying.ViewModel;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AOMBuying.Report
{
    /// <summary>
    /// Interaction logic for RPT006Page.xaml
    /// </summary>
    public partial class RPT006Page : Page
    {
        Crop _crop;
        public RPT006Page()
        {
            try
            {
                InitializeComponent();

                _crop = new Crop();
                _crop = BuyingService.CropBLL().GetDefaultCrop();
                ReloadReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void ReloadReport()
        {
            try
            {
                ReportDataSource ReportListOfBaleDetailReportViewerDataSource = new ReportDataSource();
                ReportListOfBaleDetailReportViewerDataSource.Value = Helper.BuyingHelper.GetByCrop(_crop.Crop1).Where(b => b.BuyingWeight != null);
                ReportListOfBaleDetailReportViewerDataSource.Name = "BuyingViewModelDataSet";

                ReportListOfBaleDetailReportViewer.Reset();
                ReportListOfBaleDetailReportViewer.LocalReport.DataSources.Add(ReportListOfBaleDetailReportViewerDataSource);
                ReportListOfBaleDetailReportViewer.LocalReport.ReportEmbeddedResource = "AOMBuying.Report.RDLC.RPTBUYSUM06.rdlc";
                ReportListOfBaleDetailReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
