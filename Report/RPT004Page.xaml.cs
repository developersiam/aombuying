﻿using AOMBuying.BLL;
using AOMBuying.DomainModel;
using AOMBuying.Utility;
using AOMBuying.ViewModel;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AOMBuying.Report
{
    /// <summary>
    /// Interaction logic for RPT004Page.xaml
    /// </summary>
    public partial class RPT004Page : Page
    {
        Crop _crop;
        public RPT004Page()
        {
            try
            {
                InitializeComponent();

                _crop = new Crop();
                _crop = BuyingService.CropBLL().GetDefaultCrop();
                ProjectTypeComboBox.ItemsSource = null;
                ProjectTypeComboBox.ItemsSource = BuyingService.ProjectTypeRepository().GetAll().ToList();
                ProjectTypeComboBox.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        
        private void ReloadReport()
        {
            try
            {
                var list = Helper.BuyingHelper.GetByCrop(_crop.Crop1).
                    Where(b => b.BuyingWeight != null && b.ProjectTypeID == ProjectTypeComboBox.SelectedValue.ToString()).ToList();

                ReportSummaryByPositionReportViewer.Reset();
                ReportDataSource ReportSummaryByPositionreportDataSource = new ReportDataSource();
                ReportSummaryByPositionreportDataSource.Value = list;
                ReportSummaryByPositionreportDataSource.Name = "BuyingViewModelDataSet";
                ReportSummaryByPositionReportViewer.LocalReport.DataSources.Add(ReportSummaryByPositionreportDataSource);
                ReportSummaryByPositionReportViewer.LocalReport.ReportEmbeddedResource = "AOMBuying.Report.RDLC.RPTBUYSUM04.rdlc";
                ReportSummaryByPositionReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ProjectTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ReloadReport();
        }
    }
}
