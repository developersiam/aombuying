﻿using AOMBuying.BLL;
using AOMBuying.DomainModel;
using AOMBuying.Utility;
using AOMBuying.ViewModel;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AOMBuying.Report
{
    /// <summary>
    /// Interaction logic for RPTBUY03Page.xaml
    /// </summary>
    public partial class RPT008Page : Page
    {
        Crop _crop;
        public RPT008Page()
        {
            InitializeComponent();
            _crop = new Crop();
            _crop = BuyingService.CropBLL().GetDefaultCrop();
            ReloadReport();
        }

        private void ReloadReport()
        {
            try
            {
                ReportDataSource ReportViewerDataSource = new ReportDataSource();
                ReportViewerDataSource.Value = Helper.BuyingDocumentHelper.GetByCrop(_crop.Crop1);
                ReportViewerDataSource.Name = "BuyingDocumentViewModelDataSet";

                ReportViewer.Reset();
                ReportViewer.LocalReport.DataSources.Add(ReportViewerDataSource);
                ReportViewer.LocalReport.ReportEmbeddedResource = "AOMBuying.Report.RDLC.RPTBUYSUM08.rdlc";
                ReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
