﻿using AOMBuying.BLL;
using AOMBuying.DomainModel;
using AOMBuying.Utility;
using AOMBuying.ViewModel;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AOMBuying.Report
{
    /// <summary>
    /// Interaction logic for RPT006Page.xaml
    /// </summary>
    public partial class RPT005Page : Page
    {
        Crop _crop;
        public RPT005Page()
        {
            try
            {
                InitializeComponent();

                _crop = new Crop();
                _crop = BuyingService.CropBLL().GetDefaultCrop();
                ReloadReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void ReloadReport()
        {
            try
            {
                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Value = Helper.BuyingHelper.GetByCrop(_crop.Crop1).Where(b => b.BuyingWeight != null).ToList();
                reportDataSource.Name = "BuyingViewModelDataSet";

                ReportSummaryByTransportationReportViewer.Reset();
                ReportSummaryByTransportationReportViewer.LocalReport.DataSources.Add(reportDataSource);
                ReportSummaryByTransportationReportViewer.LocalReport.ReportEmbeddedResource = "AOMBuying.Report.RDLC.RPTBUYSUM05.rdlc";
                ReportSummaryByTransportationReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
