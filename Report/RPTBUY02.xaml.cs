﻿using AOMBuying.DomainModel;
using AOMBuying.Utility;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AOMBuying.Report
{
    /// <summary>
    /// Interaction logic for RPTBUY02.xaml
    /// </summary>
    public partial class RPTBUY02 : Window
    {
        public RPTBUY02(TransportationDocument transportationDocument)
        {
            try
            {
                InitializeComponent();

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Value = Helper.BuyingHelper.GetByTransportationDocument(transportationDocument.TransportationCode);
                reportDataSource.Name = "TransportationDetails";

                TransportationVoucherReportViewer.Reset();
                TransportationVoucherReportViewer.LocalReport.DataSources.Add(reportDataSource);
                TransportationVoucherReportViewer.LocalReport.ReportEmbeddedResource = "AOMBuying.Report.RDLC.RPTBUY02.rdlc";
                TransportationVoucherReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
