﻿using AOMBuying.BLL;
using AOMBuying.DomainModel;
using AOMBuying.Utility;
using AOMBuying.ViewModel;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AOMBuying.Report
{
    /// <summary>
    /// Interaction logic for RPT002Page.xaml
    /// </summary>
    public partial class RPT002Page : Page
    {
        Crop _crop;
        public RPT002Page()
        {
            try
            {
                InitializeComponent();

                _crop = new Crop();
                _crop = BuyingService.CropBLL().GetDefaultCrop();
                ProjectTypeComboBox.ItemsSource = null;
                ProjectTypeComboBox.ItemsSource = BuyingService.ProjectTypeRepository().GetAll().ToList();
                ProjectTypeComboBox.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReloadReport()
        {
            try
            { 
                var list = Helper.FarmerQuotaHelper.GetByProject(_crop.Crop1, 
                    ProjectTypeComboBox.SelectedValue.ToString());

                var totalPrice = list.Sum(l => l.TotalPrice);

                ReportSummaryByFarmerReportViewer.Reset();
                ReportDataSource ReportSummaryByDatereportDataSource = new ReportDataSource();
                ReportSummaryByDatereportDataSource.Value = list;
                ReportSummaryByDatereportDataSource.Name = "FarmerQuotaViewModelDataSet";
                ReportSummaryByFarmerReportViewer.LocalReport.DataSources.Add(ReportSummaryByDatereportDataSource);
                ReportSummaryByFarmerReportViewer.LocalReport.ReportEmbeddedResource = "AOMBuying.Report.RDLC.RPTBUYSUM02.rdlc";
                ReportSummaryByFarmerReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ProjectTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ReloadReport();
        }
    }
}
