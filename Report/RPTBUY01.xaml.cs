﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AOMBuying.Report;
using Microsoft.Reporting.WinForms;
using AOMBuying.DomainModel;

namespace AOMBuying.Report
{
    /// <summary>
    /// Interaction logic for RPTBUY001.xaml
    /// </summary>
    public partial class RPTBUY01 : Window
    {
        public RPTBUY01(BuyingDocument bd)
        {
            try
            {
                InitializeComponent();

                BuyingVoucherReportViewer.Reset();

                //Create data table object.
                AOMBuyingSystemDataSet.BuyingDocumentDataTable BuyingDocumentDataTable = new AOMBuyingSystemDataSet.BuyingDocumentDataTable();
                AOMBuyingSystemDataSet.BuyingDetailsDataTable BuyingDetailsDataTable = new AOMBuyingSystemDataSet.BuyingDetailsDataTable();
                AOMBuyingSystemDataSet.FarmerInfoDataTable FarmerInfoDataTable = new AOMBuyingSystemDataSet.FarmerInfoDataTable();
                AOMBuyingSystemDataSet.FarmerQuotaAndSoldDataTable FarmerQuotaAndSoldDataTable = new AOMBuyingSystemDataSet.FarmerQuotaAndSoldDataTable();

                //Create table adapter object.
                AOMBuyingSystemDataSetTableAdapters.BuyingDocumentTableAdapter BuyingDocumentTableAdapter = new AOMBuyingSystemDataSetTableAdapters.BuyingDocumentTableAdapter();
                AOMBuyingSystemDataSetTableAdapters.BuyingDetailsTableAdapter BuyingDetailsTableAdapter = new AOMBuyingSystemDataSetTableAdapters.BuyingDetailsTableAdapter();
                AOMBuyingSystemDataSetTableAdapters.FarmerInfoTableAdapter FarmerInfoTableAdapter = new AOMBuyingSystemDataSetTableAdapters.FarmerInfoTableAdapter();
                AOMBuyingSystemDataSetTableAdapters.FarmerQuotaAndSoldTableAdapter FarmerQuotaAndSoldTableAdapter = new AOMBuyingSystemDataSetTableAdapters.FarmerQuotaAndSoldTableAdapter();

                //Fill data table.
                BuyingDocumentTableAdapter.Fill(BuyingDocumentDataTable, bd.Crop, bd.FarmerID, bd.DocumentNumber);
                BuyingDetailsTableAdapter.Fill(BuyingDetailsDataTable, bd.Crop, bd.FarmerID, bd.DocumentNumber);
                FarmerInfoTableAdapter.Fill(FarmerInfoDataTable, bd.Crop, bd.FarmerID);
                FarmerQuotaAndSoldTableAdapter.Fill(FarmerQuotaAndSoldDataTable, bd.Crop, bd.FarmerID);

                //Create report datat source.
                ReportDataSource BuyingDocumentDataSource = new ReportDataSource();
                ReportDataSource BuyingDetailsDataSource = new ReportDataSource();
                ReportDataSource FarmerInfoDataSource = new ReportDataSource();
                ReportDataSource FarmerQuotaAndSoldDataSource = new ReportDataSource();

                //Defind data source name.
                BuyingDocumentDataSource.Name = "BuyingDocument";
                BuyingDetailsDataSource.Name = "BuyingDetails";
                FarmerInfoDataSource.Name = "FarmerInfo";
                FarmerQuotaAndSoldDataSource.Name = "FarmerQuotaAndSold";

                //Defind data source value.
                BuyingDocumentDataSource.Value = BuyingDocumentDataTable;
                BuyingDetailsDataSource.Value = BuyingDetailsDataTable;
                FarmerInfoDataSource.Value = FarmerInfoDataTable;
                FarmerQuotaAndSoldDataSource.Value = FarmerQuotaAndSoldDataTable;

                //Binding data source to report.
                BuyingVoucherReportViewer.LocalReport.DataSources.Add(BuyingDocumentDataSource);
                BuyingVoucherReportViewer.LocalReport.DataSources.Add(BuyingDetailsDataSource);
                BuyingVoucherReportViewer.LocalReport.DataSources.Add(FarmerInfoDataSource);
                BuyingVoucherReportViewer.LocalReport.DataSources.Add(FarmerQuotaAndSoldDataSource);

                //Map report RDL parth.
                BuyingVoucherReportViewer.LocalReport.ReportEmbeddedResource = "AOMBuying.Report.RDLC.RPTBUY01.rdlc";
                BuyingVoucherReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
