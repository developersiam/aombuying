﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AOMBuying.ViewModel;
using AOMBuying.Report;
using Microsoft.Reporting.WinForms;
using AOMBuying.DomainModel;
using AOMBuying.BLL;
using AOMBuying.Utility;

namespace AOMBuying.Report
{
    /// <summary>
    /// Interaction logic for RPT001Page.xaml
    /// </summary>
    public partial class RPT001Page : Page
    {
        Crop _crop;
        public RPT001Page()
        {
            try
            {
                InitializeComponent();

                _crop = new Crop();
                _crop = BuyingService.CropBLL().GetDefaultCrop();
                ProjectTypeComboBox.ItemsSource = null;
                ProjectTypeComboBox.ItemsSource = BuyingService.ProjectTypeRepository().GetAll().ToList();
                ProjectTypeComboBox.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReloadReport()
        {
            try
            { 
                List<vm_Buying> list = new List<vm_Buying>();
                list = Helper.BuyingHelper.GetByCrop(_crop.Crop1).
                    Where(b => b.BuyingWeight != null && b.ProjectTypeID == ProjectTypeComboBox.SelectedValue.ToString()).ToList();

                ReportSummaryByCropReportViewer.Reset();
                ReportDataSource ReportSummaryByDatereportDataSource = new ReportDataSource();
                ReportSummaryByDatereportDataSource.Value = list;
                ReportSummaryByDatereportDataSource.Name = "BuyingViewModelDataSet";
                ReportSummaryByCropReportViewer.LocalReport.DataSources.Add(ReportSummaryByDatereportDataSource);
                ReportSummaryByCropReportViewer.LocalReport.ReportEmbeddedResource = "AOMBuying.Report.RDLC.RPTBUYSUM01.rdlc";
                ReportSummaryByCropReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ProjectTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ReloadReport();
        }
    }
}
