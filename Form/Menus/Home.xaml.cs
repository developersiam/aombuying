﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AOMBuying.Form.Menus
{
    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : Page
    {
        public Home()
        {
            InitializeComponent();
        }

        private void buyingFloorButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new SubMenuBuyingFloor());
        }

        private void warehouseButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new SubMenuBuyingWarehouse());
        }
    }
}
