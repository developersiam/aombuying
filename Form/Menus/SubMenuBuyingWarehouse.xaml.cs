﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AOMBuying.Form.Menus
{
    /// <summary>
    /// Interaction logic for SubMenuBuyingWarehouse.xaml
    /// </summary>
    public partial class SubMenuBuyingWarehouse : Page
    {
        public SubMenuBuyingWarehouse()
        {
            InitializeComponent();
        }

        private void buyingDocumentButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Buyings.BuyingDocuments window = new Buyings.BuyingDocuments();
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void captureBuyingInfoButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Buyings.BuyingInfo window = new Buyings.BuyingInfo();
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void exportFileButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Buyings.ExportFilesForWarehouse window = new Buyings.ExportFilesForWarehouse();
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void reprintBarcodeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Buyings.RePrintBarcode window = new Buyings.RePrintBarcode();
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
