﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AOMBuying.DomainModel;
using AOMBuying.BLL;
using AOMBuying.ViewModel;

namespace AOMBuying.Form.Buyings
{
    /// <summary>
    /// Interaction logic for PrintBarcodePage.xaml
    /// </summary>
    public partial class BuyingDocumentCreate : Window
    {
        public BuyingDocumentCreate(DateTime buyingDate, Guid buyingStationID)
        {
            try
            {
                InitializeComponent();

                buyingDateDatePicker.SelectedDate = buyingDate;
                buyingDateDatePicker.IsEnabled = false;

                buyingStationComboBox.ItemsSource = null;
                buyingStationComboBox.ItemsSource = BuyingService.BuyingStationBLL()
                    .GetAll()
                    .OrderBy(b => b.BuyingStationName);

                buyingStationComboBox.SelectedValue = buyingStationID.ToString();
                buyingStationComboBox.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CreateDocument()
        {
            try
            {
                if (buyingStationComboBox.SelectedIndex < 0)
                    throw new ArgumentException("Please select a buying station!");

                if (farmerIDTextBox.Text == "")
                {
                    MessageBox.Show("Please input a farmer id.", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    farmerIDTextBox.Text = "";
                    farmerIDTextBox.Focus();
                    return;
                }

                if (requestBarcodeTextBox.Text == "")
                {
                    MessageBox.Show("Please input a number of request barcode.", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    requestBarcodeTextBox.Text = "";
                    requestBarcodeTextBox.Focus();
                    return;
                }

                var list = BuyingService.FarmerProjectBLL()
                    .GetByFarmer(sys_config.DefaultCrop.Crop1, farmerIDTextBox.Text);

                if (list.Where(p => p.ProjectTypeID == projectTypeTextBox.Text).Count() <= 0)
                    throw new ArgumentException("This farmer not have a quota in " + projectTypeTextBox.Text + " project.");

                //Create new buying document.
                BuyingDocument newBuyingDocument = new BuyingDocument
                {
                    Crop = sys_config.DefaultCrop.Crop1,
                    FarmerID = farmerIDTextBox.Text,
                    BuyingStationID = ((BuyingStation)buyingStationComboBox.SelectedItem).BuyingStationID,
                    CreateUser = sys_config.CurrentUser,
                    CreateDate = Convert.ToDateTime(buyingDateDatePicker.SelectedDate),
                    ModifiedDate = DateTime.Now,
                    ModifiedUser = sys_config.CurrentUser,
                    IsFinish = false
                };

                BuyingDocument newDocument = BuyingService.BuyingDocumentBLL()
                    .Add(newBuyingDocument);

                for (int i = 1; i <= Convert.ToInt16(requestBarcodeTextBox.Text); i++)
                    BuyingService.BuyingBLL().Add(newDocument,
                        projectTypeTextBox.Text, sys_config.CurrentUser);

                BuyingDocumentDetails window = new BuyingDocumentDetails(newDocument);
                window.ShowDialog();

                //Refresh aby data on the screen.
                FarmerProfileBinding(sys_config.DefaultCrop.Crop1, farmerIDTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void FarmerProfileBinding(short crop, string farmerID)
        {
            try
            {
                //Get farmer profile from database for display data.
                var farmerRegis = BuyingService.FarmerRegistrationBLL()
                    .GetFarmerRegistrationByID(sys_config.DefaultCrop.Crop1, farmerID);

                if (farmerRegis == null)
                {
                    MessageBox.Show("Find farmer not found!", "Message", MessageBoxButton.OK, MessageBoxImage.Information);
                    ClearForm();
                    return;
                }

                //Bind farmer profile into input control.
                farmerIDTextBox.Text = farmerRegis.Farmer.FarmerID;
                farmerNameTextBox.Text = farmerRegis.Farmer.FarmerName;
                nrcNoTextBox.Text = farmerRegis.Farmer.NRCNo;

                //Farmer crop quota data binding.
                var farmerQuota = Helper.FarmerQuotaHelper.GetByFarmer(farmerRegis.Crop, farmerRegis.FarmerID);
                projectTypeTextBox.Text = farmerQuota.FirstOrDefault().ProjectTypeID;

                //Buying document list data binding.
                var documentList = Helper.BuyingDocumentHelper.GetByFarmer(crop, farmerID);

                buyingDocumentDataGrid.ItemsSource = null;
                buyingDocumentDataGrid.ItemsSource = documentList;

                totalItemLabel.Content = documentList.Count.ToString("N0");

                //Buying summary data binding.
                double totalQuota = farmerQuota.Sum(f => f.Quota);
                double totalSold = Convert.ToDouble(documentList.Sum(bd => bd.TotalWeight));
                double totalBalance = totalQuota - totalSold;

                totalQuotaTextBox.Text = totalQuota.ToString("N0");
                totalWeightSoldTextBox.Text = totalSold.ToString("N0");
                balanceKgTextBox.Text = totalBalance.ToString("N0");
                requestBarcodeTextBox.Text = "";
                requestBarcodeTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearForm()
        {
            try
            {
                farmerIDTextBox.Text = "";
                farmerNameTextBox.Text = "";
                nrcNoTextBox.Text = "";
                projectTypeTextBox.Text = "";
                totalQuotaTextBox.Text = "";
                totalWeightSoldTextBox.Text = "";
                balanceKgTextBox.Text = "";
                buyingDocumentDataGrid.ItemsSource = null;
                farmerIDTextBox.Text = "";
                farmerIDTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void viewDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (buyingDocumentDataGrid.SelectedIndex < 0)
                    return;

                var item = (vm_BuyingDocument)buyingDocumentDataGrid.SelectedItem;
                //Display a print barcode page.
                BuyingDocumentDetails window = new BuyingDocumentDetails(BuyingService.BuyingDocumentBLL()
                .GetSingle(item.Crop, item.FarmerID, item.DocumentNumber));

                window.ShowDialog();
                FarmerProfileBinding(sys_config.DefaultCrop.Crop1, farmerIDTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (buyingDocumentDataGrid.SelectedIndex < 0)
                    return;

                if (MessageBox.Show("Do you want to delete this document?", "Warning",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                var item = (vm_BuyingDocument)buyingDocumentDataGrid.SelectedItem;
                BuyingService.BuyingDocumentBLL()
                    .Delete(new BuyingDocument
                    {
                        Crop = item.Crop,
                        FarmerID = item.FarmerID,
                        DocumentNumber = item.DocumentNumber
                    });

                FarmerProfileBinding(sys_config.DefaultCrop.Crop1, farmerIDTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void requestBarcodeTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                CreateDocument();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void createDocumentButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CreateDocument();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Window_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Escape)
                    ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void farmerIDTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if(e.Key == Key.Enter)
                    FarmerProfileBinding(sys_config.DefaultCrop.Crop1, farmerIDTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            farmerIDTextBox.Focus();
        }
    }
}
