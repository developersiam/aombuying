﻿using AOMBuying.BLL;
using AOMBuying.DomainModel;
using AOMBuying.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AOMBuying.Form.Buyings
{
    /// <summary>
    /// Interaction logic for PrintBarcodePage.xaml
    /// </summary>
    public partial class BuyingDocumentDetails : Window
    {
        BuyingDocument _buyingDocument;
        List<Buying> _buyingList;

        public BuyingDocumentDetails(BuyingDocument model)
        {
            try
            {
                InitializeComponent();
                _buyingDocument = new BuyingDocument();
                _buyingList = new List<Buying>();

                _buyingDocument = model;
                BuyingDocumentBinding();
                BuyingDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BuyingDocumentBinding()
        {
            try
            {
                var model = Helper.BuyingDocumentHelper
                    .GetSingle(_buyingDocument.Crop,
                    _buyingDocument.FarmerID,
                    _buyingDocument.DocumentNumber);

                documentCodeTextBox.Text = model.BuyingDocumentCode;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BuyingDataGridBinding()
        {
            try
            {
                _buyingList = BuyingService.BuyingBLL()
                    .GetByDocument(_buyingDocument)
                    .OrderByDescending(b => b.BaleNumber)
                    .ToList();

                buyingDataGrid.ItemsSource = null;
                buyingDataGrid.ItemsSource = _buyingList;

                totalItemLabel.Content = _buyingList.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void rePrintButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Go to re-print page.
                if (buyingDataGrid.SelectedIndex < 0)
                    return;

                if (MessageBox.Show("Do you want to re-print this bale barcode?", "Warning",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BuyingService.BuyingBLL().PrintBaleBarcodeSticker(((Buying)buyingDataGrid.SelectedItem).BaleBarcode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void printBarcodeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Call print barcode function.
                if (MessageBox.Show("Do you want to print " + _buyingList.Count.ToString("N0") + " barcode?", "warning",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;
                foreach (var item in _buyingList)
                    BuyingService.BuyingBLL().PrintBaleBarcodeSticker(item.BaleBarcode);
                //MessageBox.Show("print " + item.BaleBarcode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (buyingDataGrid.SelectedIndex < 0)
                    return;

                if (MessageBox.Show("Do you want to delete this bale?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BuyingService.BuyingBLL().Delete(((Buying)buyingDataGrid.SelectedItem).BaleBarcode);
                BuyingDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void printBuyingVoucherButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_buyingDocument.IsFinish == false)
                {
                    MessageBox.Show("Please lock this document before print.", "message", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                // Go to print preview page.
                Report.RPTBUY01 window = new Report.RPTBUY01(new vm_BuyingDocument
                {
                    Crop = _buyingDocument.Crop,
                    FarmerID = _buyingDocument.FarmerID,
                    DocumentNumber = _buyingDocument.DocumentNumber
                });
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void printVoucherButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to print a voucher?", "confirmation", 
                    MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                    return;

                if(_buyingList.Where(x=>x.RejectTypeID == null && x.BuyingWeight == null).Count() > 0)
                {
                    MessageBox.Show("This document have a " 
                        + _buyingList.Where(x => x.RejectTypeID == null && x.BuyingWeight == null).Count() 
                        + " bales was not record the buying weight.You cannot finish.", "warning!", 
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (_buyingDocument.IsFinish == false)
                    BuyingService.BuyingDocumentBLL().Finish(_buyingDocument);

                Report.RPTBUY01 window = new Report.RPTBUY01(_buyingDocument);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
