﻿using AOMBuying.BLL;
using AOMBuying.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AOMBuying.Form.Buyings
{
    /// <summary>
    /// Interaction logic for ExportFilesForWarehouse.xaml
    /// </summary>
    public partial class ExportFilesForWarehouse : Window
    {
        List<vm_Buying> _buyingList;
        public ExportFilesForWarehouse()
        {
            try
            {
                InitializeComponent();

                _buyingList = new List<vm_Buying>();
                buyingDatePicker.SelectedDate = DateTime.Now;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BuyingDataGridBinding()
        {
            try
            {
                var list = _buyingList.Where(x => x.TransportationCode == null).ToList();
                buyingDataGrid.ItemsSource = null;
                buyingDataGrid.ItemsSource = list;

                totalBuyingItemLabel.Content = list.Count().ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ExportationDataGridBinding()
        {
            try
            {
                if (transportationCodeComboBox.SelectedValue == null)
                {
                    exportDataGrid.ItemsSource = null;
                    return;
                }

                var list = _buyingList
                    .Where(x => x.TransportationCode == transportationCodeComboBox.SelectedValue.ToString())
                    .ToList();

                exportDataGrid.ItemsSource = null;
                exportDataGrid.ItemsSource = list;

                totalMovedItemLabel.Content = list.Count().ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void TransportationDocumentBinding()
        {
            try
            {
                if (buyingDatePicker.SelectedDate == null)
                    return;

                transportationCodeComboBox.ItemsSource = null;
                transportationCodeComboBox.ItemsSource = BuyingService.TransportationBLL()
                    .GetByCreateDate(Convert.ToDateTime(buyingDatePicker.SelectedDate))
                    .Where(x => x.TransportationCode.Contains("WH"));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SummaryDataBinding()
        {
            try
            {
                if (buyingDatePicker.SelectedDate == null)
                    return;

                _buyingList = Helper.BuyingHelper
                    .GetByBuyingDate(Convert.ToDateTime(buyingDatePicker.SelectedDate));

                var groupByList = _buyingList
                    .GroupBy(x => x.BuyingGrade)
                    .Select(x => new
                    {
                        BuyingGrade = x.Key,
                        TotalBale = x.Count(),
                        TotalWeight = x.Sum(y => y.BuyingWeight)
                    }).ToList();

                summaryBuyingDataGrid.ItemsSource = null;
                summaryBuyingDataGrid.ItemsSource = groupByList;

                totalBaleTextBox.Text = _buyingList.Count.ToString("N0");
                totalEmptyBaleTextBox.Text = _buyingList.Where(x => x.BuyingGrade == null && x.RejectTypeID == null).Count().ToString("N0");
                totalBuyingTextBox.Text = _buyingList.Where(x => x.BuyingGrade != null).Count().ToString("N0");
                totalRejectedTextBox.Text = _buyingList.Where(x => x.RejectTypeID != null).Count().ToString("N0");
                totalWeightTextBox.Text = Convert.ToDecimal(_buyingList.Sum(x => x.BuyingWeight)).ToString("N1");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void transportationCodeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (transportationCodeComboBox.SelectedIndex < 0)
                    return;

                ExportationDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void moveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //if(buyingDataGrid.SelectedItems.Count <= 0)
                //{
                //    MessageBox.Show("Please select a buying row from the left table.", "warning", 
                //        MessageBoxButton.OK, MessageBoxImage.Warning);
                //    buyingDataGrid.Focus();
                //    return;
                //}

                if (transportationCodeComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("Please select a exportation code.", "warning",
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                    transportationCodeComboBox.Focus();
                    return;
                }

                var list = _buyingList.Where(x => x.TransportationCode == null && x.BuyingWeight != null);

                if (MessageBox.Show("Do you want to move " + list.Count() +
                    " bale to the exportation code " +
                    transportationCodeComboBox.SelectedValue.ToString() + "?", "warning",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                foreach (var item in list)
                {
                    BuyingService.TransportationBLL().BringUp(item.BaleBarcode,
                        sys_config.CurrentUser, transportationCodeComboBox.SelectedValue.ToString());
                }

                SummaryDataBinding();
                BuyingDataGridBinding();
                ExportationDataGridBinding();

                MessageBox.Show("Moved the bale to exportation code already!", "Done", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void moveBackButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (exportDataGrid.SelectedItems.Count <= 0)
                {
                    MessageBox.Show("Please select a buying row from the right table.", "warning",
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                    exportDataGrid.Focus();
                    return;
                }

                foreach (var item in exportDataGrid.SelectedItems)
                {
                    var model = (vm_Buying)item;
                    BuyingService.TransportationBLL().BringDown(model.BaleBarcode);
                }

                SummaryDataBinding();
                BuyingDataGridBinding();
                ExportationDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void showButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SummaryDataBinding();
                BuyingDataGridBinding();
                TransportationDocumentBinding();
                ExportationDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void createDocButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to crate a new exportation document?", "warning",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                var transportationCode = BuyingService.TransportationBLL()
                    .Add(sys_config.DefaultCrop.Crop1, null,
                    Convert.ToDateTime(buyingDatePicker.SelectedDate),
                    sys_config.CurrentUser);

                TransportationDocumentBinding();
                MessageBox.Show("Create a new exportation code " + transportationCode + " already.",
                    "Done", MessageBoxButton.OK, MessageBoxImage.Information);

                transportationCodeComboBox.SelectedValue = transportationCode;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void deleteDocButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (transportationCodeComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("Please select exportation code", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    transportationCodeComboBox.Focus();
                    return;
                }

                if (MessageBox.Show("Do you want to delete the exportation code " + 
                    transportationCodeComboBox.SelectedValue.ToString() + "?", "warning",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                if (_buyingList.Where(x => x.TransportationCode == transportationCodeComboBox.SelectedValue.ToString()).Count() > 0)
                {
                    MessageBox.Show("This exportation code have a bale contained. Cannot delete!",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                BuyingService.TransportationBLL().Delete(transportationCodeComboBox.SelectedValue.ToString());
                TransportationDocumentBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void buyingDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (buyingDataGrid.SelectedIndex < 0)
                    return;

                var item = (vm_Buying)buyingDataGrid.SelectedItem;

                if (item.BuyingGrade == null)
                {
                    MessageBox.Show("Cannot move. Because this bale not have a buying grade.",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    buyingDataGrid.UnselectAllCells();
                    //moveSelectedItemLabel.Content = 0;
                    return;
                }

                //moveSelectedItemLabel.Content = buyingDataGrid.SelectedItems.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void exportDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (exportDataGrid.SelectedIndex < 0)
                    return;

                movedBackSelectedItemLabel.Content = exportDataGrid.SelectedItems.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void exportFileButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (exportDataGrid.Items.Count <= 0)
                {
                    MessageBox.Show("Cannot export an empty rows. Please select row from the left table.",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (transportationCodeComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("Please select exportation code.",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    transportationCodeComboBox.Focus();
                    return;
                }

                System.Windows.Forms.FolderBrowserDialog folderDlg = new System.Windows.Forms.FolderBrowserDialog();
                folderDlg.ShowNewFolderButton = true;

                // Show the FolderBrowserDialog.
                System.Windows.Forms.DialogResult result = folderDlg.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    string folderPath = Helper.TransportationHelper
                        .ExportTransportationDocument(transportationCodeComboBox.SelectedValue.ToString(), folderDlg.SelectedPath);
                    Environment.SpecialFolder root = folderDlg.RootFolder;
                    MessageBox.Show("Export complete.", "Complete", MessageBoxButton.OK, MessageBoxImage.Information);
                    System.Diagnostics.Process.Start(folderPath);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
