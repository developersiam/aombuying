﻿using AOMBuying.BLL;
using AOMBuying.DomainModel;
using AOMBuying.Utility;
using AOMBuying.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AOMBuying.Form.Buyings
{
    /// <summary>
    /// Interaction logic for RePrintBarcodePage.xaml
    /// </summary>
    public partial class RePrintBarcode : Window
    {
        Buying _buying;

        public RePrintBarcode()
        {
            try
            {
                InitializeComponent();
                _buying = new Buying();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                BaleBarcodeTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DisplayBaleInfo()
        {
            try
            {
                BaleBarcodeTextBox.Text = _buying.BaleBarcode;
                BaleNumberTextBox.Text = _buying.BaleNumber.ToString();
                FarmerIDTextBox.Text = _buying.FarmerID;
                ProjectTypeTextBox.Text = _buying.ProjectTypeID;
                BuyingGradeTextBox.Text = _buying.BuyingGrade;
                BuyingWeightTextBox.Text = string.Format("{0:N2}", _buying.BuyingWeight);

                if (_buying.TransportationCode != null)
                    TransportationCodeTextBox.Text = "TR-" +
                            _buying.TransportationDocument.CreateDate.Year +
                            _buying.TransportationDocument.CreateDate.Month +
                            _buying.TransportationDocument.CreateDate.Day + "-" +
                            _buying.TransportationDocument.TruckNo;

                LoadToTruckDateTextBox.Text = _buying.LoadBaleToTruckDate.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DisplayBaleBarcodeInfo(string baleBarcode)
        {
            try
            {
                if (baleBarcode == "")
                {
                    MessageBox.Show("Please input a bale barcode.", "Message", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Focus();
                    return;
                }

                _buying = BuyingService.BuyingBLL().GetByBaleBarcode(baleBarcode);
                if (_buying == null)
                {
                    MessageBox.Show("Find the bale not found!", "Message", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Text = "";
                    BaleBarcodeTextBox.Focus();
                    return;
                }
                DisplayBaleInfo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DisplayBaleBarcodeInfoByBaleNumber(short baleNumber)
        {
            try
            {
                _buying = BuyingService.BuyingBLL().GetByBaleNumber(baleNumber);
                if (_buying == null)
                {
                    MessageBox.Show("Find the bale not found!", "Message", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Text = "";
                    BaleNumberTextBox.Text = "";
                    BaleNumberTextBox.Focus();
                    return;
                }
                DisplayBaleInfo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BaleBarcodeTextBox.Text = "";
                BaleNumberTextBox.Text = "";
                FarmerIDTextBox.Text = "";
                ProjectTypeTextBox.Text = "";
                BuyingGradeTextBox.Text = "";
                BuyingWeightTextBox.Text = "";
                TransportationCodeTextBox.Text = "";
                LoadToTruckDateTextBox.Text = "";
                BaleBarcodeTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void PrintBarcodeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BuyingService.BuyingBLL().PrintBaleBarcodeSticker(_buying.BaleBarcode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BaleBarcodeTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                    DisplayBaleBarcodeInfo(BaleBarcodeTextBox.Text.Replace(" ", string.Empty));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BaleNumberTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                    DisplayBaleBarcodeInfoByBaleNumber(Convert.ToInt16(BaleNumberTextBox.Text.Replace(" ", string.Empty)));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
