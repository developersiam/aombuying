﻿using AOMBuying.BLL;
using AOMBuying.DomainModel;
using AOMBuying.Utility;
using AOMBuying.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AOMBuying.Form.Buyings
{
    /// <summary>
    /// Interaction logic for BuyingDocumentListPage.xaml
    /// </summary>
    public partial class BuyingDocuments : Window
    {
        public BuyingDocuments()
        {
            try
            {
                InitializeComponent();

                createDateDatePicker.SelectedDate = DateTime.Now;
                buyingStationComboBox.ItemsSource = null;
                buyingStationComboBox.ItemsSource = BuyingService.BuyingStationBLL().GetAll();

                buyingDocumentDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void buyingDocumentDataGridBinding()
        {
            try
            {
                if (createDateDatePicker.SelectedDate == null)
                    return;

                if (buyingStationComboBox.SelectedIndex < 0)
                    return;

                var station = (BuyingStation)buyingStationComboBox.SelectedItem;

                var list = Helper.BuyingDocumentHelper
                    .GetByCreateDate(Convert.ToDateTime(createDateDatePicker.SelectedDate))
                    .Where(x => x.BuyingStationID == station.BuyingStationID);

                buyingDocumentDataGrid.ItemsSource = null;
                buyingDocumentDataGrid.ItemsSource = list;

                totalItemLabel.Content = list.Count().ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void viewDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (buyingDocumentDataGrid.SelectedIndex < 0)
                    return;

                var item = (vm_BuyingDocument)buyingDocumentDataGrid.SelectedItem;

                //Display a print barcode page.
                BuyingDocumentDetails window = new BuyingDocumentDetails(new BuyingDocument
                {
                    Crop = item.Crop,
                    FarmerID = item.FarmerID,
                    BuyingStationID = item.BuyingStationID,
                    DocumentNumber = item.DocumentNumber,
                    IsFinish = item.IsFinish
                });

                window.ShowDialog();
                buyingDocumentDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (buyingDocumentDataGrid.SelectedIndex < 0)
                    return;

                if (MessageBox.Show("Do you want to delete this document?", "Warning",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                var item = (vm_BuyingDocument)buyingDocumentDataGrid.SelectedItem;
                BuyingService.BuyingDocumentBLL().Delete(new BuyingDocument
                {
                    Crop = item.Crop,
                    FarmerID = item.FarmerID,
                    DocumentNumber = item.DocumentNumber
                });

                buyingDocumentDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void createDateDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                buyingDocumentDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void createDocumentButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (createDateDatePicker.SelectedDate == null)
                    throw new ArgumentException("Please select a createt date.");

                if (buyingStationComboBox.SelectedIndex < 0)
                    throw new ArgumentException("Please select a buying station.");

                BuyingDocumentCreate window = new BuyingDocumentCreate(
                    Convert.ToDateTime(createDateDatePicker.SelectedDate),
                    Guid.Parse(buyingStationComboBox.SelectedValue.ToString()));
                window.ShowDialog();

                buyingDocumentDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void buyingStationComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            buyingDocumentDataGridBinding();
        }
    }
}
