﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AOMBuying.DomainModel;
using AOMBuying.BLL;
using AOMBuying.ViewModel;

namespace AOMBuying.Form.Buyings
{
    /// <summary>
    /// Interaction logic for BuyingPage.xaml
    /// </summary>
    public partial class BuyingInfo : Window
    {
        Buying _buying;
        vm_FarmerCropQuota _farmerQuota;
        List<vm_Buying> _buyingList;
        List<BuyingGrade> _buyinGradeList;

        public BuyingInfo()
        {
            try
            {
                InitializeComponent();

                _buying = new Buying();
                _farmerQuota = new vm_FarmerCropQuota();
                _buyingList = new List<vm_Buying>();
                _buyinGradeList = new List<BuyingGrade>();

                _buyinGradeList = BuyingService.BuyingGradeBLL()
                    .GetBuyingGradeByCrop(sys_config.DefaultCrop.Crop1)
                    .OrderBy(g => g.BuyingGrade1).ToList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Save()
        {
            try
            {
                if (baleBarcodeTextBox.Text == "")
                {
                    MessageBox.Show("Please input a bale barcode.", "Message", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Focus();
                    return;
                }

                if (baleNumberTextBox.Text == "")
                {
                    MessageBox.Show("Please input a bale number.", "Message", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleNumberTextBox.Focus();
                    return;
                }

                if (buyingGradeComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("Please input a buying grade.", "Message", MessageBoxButton.OK, MessageBoxImage.Warning);
                    buyingGradeComboBox.Focus();
                    return;
                }

                if (buyingWeightTextBox.Text == "")
                {
                    MessageBox.Show("Please input a buying weight.", "Message", MessageBoxButton.OK, MessageBoxImage.Warning);
                    buyingWeightTextBox.Focus();
                    return;
                }

                if (Helper.RegularExpressionHelper.IsDecimalCharacter(buyingWeightTextBox.Text) == false)
                {
                    MessageBox.Show("The buying weight should be decimal.", "Message", MessageBoxButton.OK, MessageBoxImage.Warning);
                    buyingWeightTextBox.Focus();
                    return;
                }

                if (Helper.RegularExpressionHelper.IsNumericCharacter(baleNumberTextBox.Text) == false)
                {
                    MessageBox.Show("The bale number should be numeric.", "Message", MessageBoxButton.OK, MessageBoxImage.Warning);
                    buyingWeightTextBox.Focus();
                    return;
                }

                if (Convert.ToDecimal(buyingWeightTextBox.Text) > 100)
                    if (MessageBox.Show("The buying weight more than 100 kg.Do you want to buying this bale?", "warning!",
                        MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                        return;

                if (Convert.ToDecimal(buyingWeightTextBox.Text) < 10)
                    if (MessageBox.Show("The buying weight less than 10 kg.Do you want to buying this bale?", "warning!",
                        MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                        return;

                if (Convert.ToDecimal(buyingWeightTextBox.Text) <= 0)
                {
                    MessageBox.Show("Please input a buying weight more than 0 kg.", "Message", MessageBoxButton.OK, MessageBoxImage.Warning);
                    buyingWeightTextBox.Focus();
                    return;
                }

                /// Get bale info by bale barcode from database for check dupplicated barcode.
                _buying = BuyingService.BuyingBLL().GetByBaleBarcode(baleBarcodeTextBox.Text);

                if (_buying == null)
                {
                    MessageBox.Show("Find the bale not found!", "Message", MessageBoxButton.OK, MessageBoxImage.Warning);
                    Clear();
                    return;
                }

                if (_farmerQuota.KgSold + Convert.ToDouble(buyingWeightTextBox.Text) > _farmerQuota.Quota)
                    if (MessageBox.Show("This farmer quota is full. Do you want to accept?", "warning",
                        MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                        return;

                if(_buying.RejectTypeID != null)
                {
                    if (MessageBox.Show("This bale was rejected. Do you want to cancel rejection and buying again?", "warning",
                        MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                        return;
                }

                // Svae to a database.
                BuyingService.BuyingBLL().CaptureBuyingInformation(_buying.BaleBarcode,
                        buyingGradeComboBox.Text.Replace(" ", string.Empty),
                        Convert.ToDecimal(buyingWeightTextBox.Text.Replace(" ", string.Empty)),
                        sys_config.CurrentUser,
                        DateTime.Now,
                        _buying.BuyingDocument);

                ReloadBuyingDocument();
                ReloadBuyingList();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Clear()
        {
            try
            {
                baleBarcodeTextBox.Text = "";
                baleNumberTextBox.Text = "";
                buyingGradeComboBox.SelectedIndex = -1;
                buyingWeightTextBox.Text = "";
                saveButton.Content = "Save";
                baleBarcodeTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReloadBuyingDocument()
        {
            try
            {
                var farmerProfile = BuyingService.FarmerBLL().GetSingle(_buying.FarmerID);

                farmerIDTextBox.Text = farmerProfile.FarmerID;
                farmerNameTextBox.Text = farmerProfile.FarmerName;

                _farmerQuota = Helper.FarmerQuotaHelper
                    .GetByFarmer(_buying.Crop, _buying.FarmerID)
                    .SingleOrDefault(x => x.ProjectTypeID == _buying.ProjectTypeID);

                if (_farmerQuota != null)
                {
                    projectTypeTextBox.Text = _farmerQuota.ProjectTypeID;
                    totalQuotaTextBox.Text = string.Format("{0:N0}", _farmerQuota.Quota);
                    totalSoldTextBox.Text = string.Format("{0:N0}", _farmerQuota.KgSold);
                    balanceTextBox.Text = string.Format("{0:N0}", _farmerQuota.KgBalance);
                }

                var document = Helper.BuyingDocumentHelper
                    .GetSingle(_buying.Crop, _buying.FarmerID, _buying.DocumentNumber);

                documentCodeTextBox.Text = document.BuyingDocumentCode;
                totalBaleTextBox.Text = document.TotalBale.ToString("N0");
                totalWeightTextBox.Text = document.TotalWeight.ToString("N0");
                totalPriceTextBox.Text = document.TotalPrice.ToString("N0");
                finishStatusCheckBox.IsChecked = document.IsFinish;
                finishDateTextBox.Text = document.FinishDate.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReloadBuyingList()
        {
            try
            {
                _buyingList = Helper.BuyingHelper
                    .GetByBuyingDocument(new vm_BuyingDocument
                    {
                        Crop = _buying.Crop,
                        FarmerID = _buying.FarmerID,
                        BuyingStationID = _buying.BuyingDocument.BuyingStationID,
                        DocumentNumber = _buying.DocumentNumber
                    })
                    .OrderByDescending(b => b.BuyingWeightRecordDate)
                    .ToList();

                BuyingDataGrid.ItemsSource = null;
                BuyingDataGrid.ItemsSource = _buyingList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DisplayBaleBarcodeInfo(string baleBarcode)
        {
            try
            {
                if (baleBarcode == "")
                {
                    MessageBox.Show("Please input a bale barcode.", "Message", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Focus();
                    return;
                }

                /// Get a bale barcode info from database for check a dupplicated barcode.
                _buying = BuyingService.BuyingBLL().GetByBaleBarcode(baleBarcode);

                if (_buying == null)
                {
                    MessageBox.Show("Find the bale not found!", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Text = "";
                    baleBarcodeTextBox.Focus();
                    return;
                }

                buyingGradeComboBox.ItemsSource = null;
                buyingGradeComboBox.ItemsSource = _buyinGradeList
                    .Where(g => g.ProjectTypeID == _buying.ProjectTypeID)
                    .OrderBy(o => o.BuyingGrade1).ToList();

                ReloadBuyingDocument();
                ReloadBuyingList();

                baleBarcodeTextBox.Text = _buying.BaleBarcode;
                baleNumberTextBox.Text = _buying.BaleNumber.ToString();
                buyingGradeComboBox.SelectedValue = _buying.BuyingGrade;
                buyingWeightTextBox.Text = string.Format("{0:N2}", _buying.BuyingWeight);
                saveButton.Content = _buying.BuyingGrade1 == null || _buying.BuyingWeight == null ? "Save" : "Edit";
                buyingGradeComboBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DisplayBaleBarcodeInfoByBaleNumber(short baleNumber)
        {
            try
            {
                /// Get a bale barcode info from database for check a dupplicated barcode.
                _buying = BuyingService.BuyingBLL().GetByBaleNumber(baleNumber);

                if (_buying == null)
                {
                    MessageBox.Show("Find the bale not found!", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleNumberTextBox.Text = "";
                    baleNumberTextBox.Focus();
                    return;
                }

                buyingGradeComboBox.ItemsSource = null;
                buyingGradeComboBox.ItemsSource = _buyinGradeList
                    .Where(g => g.ProjectTypeID == _buying.ProjectTypeID)
                    .OrderBy(o => o.BuyingGrade1).ToList();
                ReloadBuyingDocument();
                ReloadBuyingList();

                baleBarcodeTextBox.Text = _buying.BaleBarcode;
                baleNumberTextBox.Text = _buying.BaleNumber.ToString();
                buyingGradeComboBox.SelectedValue = _buying.BuyingGrade;
                buyingWeightTextBox.Text = string.Format("{0:N2}", _buying.BuyingWeight);
                saveButton.Content = _buying.BuyingGrade1 == null || _buying.BuyingWeight == null ? "Save" : "Edit";
                buyingGradeComboBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void baleBarcodeTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                DisplayBaleBarcodeInfo(baleBarcodeTextBox.Text.Replace(" ", string.Empty));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void baleNumberTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                DisplayBaleBarcodeInfoByBaleNumber(Convert.ToInt16(baleNumberTextBox.Text.Replace(" ", string.Empty)));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void buyingWeightTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                    Save();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (saveButton.Content.ToString() == "Edit")
                    if (MessageBox.Show("Do you want to edit this item?", "warning",
                        MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                        return;
                Save();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BuyingDataGrid.SelectedIndex < 0)
                    return;

                var selectedItem = ((vm_Buying)BuyingDataGrid.SelectedItem);
                DisplayBaleBarcodeInfo(selectedItem.BaleBarcode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BuyingDataGrid.SelectedIndex < 0)
                    return;

                if (MessageBox.Show("Do you want to delete this item?", "warning!",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                var selectedItem = ((vm_Buying)BuyingDataGrid.SelectedItem);
                BuyingService.BuyingBLL().Delete(selectedItem.BaleBarcode);

                ReloadBuyingList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void buyingGradeComboBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                    buyingWeightTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Window_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Clear();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            baleBarcodeTextBox.Focus();
        }

        private void rejectButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_buying == null)
                {
                    MessageBox.Show("Find the bale barcode not found. Please scan the barcode and try to reject again.", 
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (MessageBox.Show("Do you want to reject this item?", "warning!",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                var result = Rejects.ReturnRejectType();
                if (result == null)
                {
                    MessageBox.Show("Not found a reject type. Please select reject type again.", "warning!", 
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                BuyingService.BuyingBLL().RejectBale(_buying.BaleBarcode, result.RejectTypeID);
                ReloadBuyingList();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
