﻿using AOMBuying.BLL;
using AOMBuying.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AOMBuying.Form.Buyings
{
    /// <summary>
    /// Interaction logic for Rejects.xaml
    /// </summary>
    public partial class Rejects : Window
    {
        public static RejectType _rejectType;

        public Rejects()
        {
            InitializeComponent();

            _rejectType = new RejectType();

            rejectTypeItemsControl.ItemsSource = null;
            rejectTypeItemsControl.ItemsSource = BuyingService.RejectTypeBLL().GetAll().OrderBy(x => x.RejectTypeName);
        }

        public static RejectType ReturnRejectType()
        {
            Rejects window = new Rejects();
            window.ShowDialog();

            return _rejectType;
        }

        private void rejectTypeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                _rejectType = BuyingService.RejectTypeBLL().GetSingle(Guid.Parse(btn.ToolTip.ToString()));

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
