﻿using AOMBuying.BLL;
using AOMBuying.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AOMBuying.Form.Setups
{
    /// <summary>
    /// Interaction logic for SetupBuyingStationPage.xaml
    /// </summary>
    public partial class SetupBuyingStationPage : Page
    {
        BuyingStation _buyingStation;
        List<BuyingStation> _buyingStationList;

        public SetupBuyingStationPage()
        {
            try
            {
                InitializeComponent();

                _buyingStation = new BuyingStation();
                _buyingStationList = new List<BuyingStation>();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearForm()
        {
            BuyingStationNameTextBox.Text = "";
            BuyingStationNameTextBox.Focus();
            SaveButton.Content = "Save";
        }

        private void ReloadBuyingStationList()
        {
            try
            {
                _buyingStationList = BuyingService.BuyingStationBLL()
                    .GetAll()
                    .OrderBy(s => s.BuyingStationName).ToList();

                BuyingStationDataGrid.ItemsSource = null;
                BuyingStationDataGrid.ItemsSource = _buyingStationList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            ReloadBuyingStationList();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if(BuyingStationNameTextBox.Text == "")
                {
                    MessageBox.Show("Please input buying station name.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BuyingStationNameTextBox.Focus();
                    return;
                }
                if(SaveButton.Content.ToString() == "Save")
                {
                    BuyingService.BuyingStationBLL().Add(new BuyingStation
                    {
                        BuyingStationID = Guid.NewGuid(),
                        BuyingStationName = BuyingStationNameTextBox.Text
                    });
                }
                else
                {
                    _buyingStation.BuyingStationName = BuyingStationNameTextBox.Text;
                    BuyingService.BuyingStationBLL().Edit(_buyingStation);
                }
                ClearForm();
                ReloadBuyingStationList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BuyingStationDataGrid.SelectedIndex < 0)
                    return;
                var selectedItem = (BuyingStation)BuyingStationDataGrid.SelectedItem;
                _buyingStation = selectedItem;
                BuyingStationNameTextBox.Text = selectedItem.BuyingStationName;
                SaveButton.Content = "Edit";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BuyingStationDataGrid.SelectedIndex < 0)
                    return;
                if (MessageBox.Show("Do you want to delete this item?", "warning!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                var selectedItem = (BuyingStation)BuyingStationDataGrid.SelectedItem;
                BuyingService.BuyingStationBLL().Delete(selectedItem);

                ClearForm();
                ReloadBuyingStationList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
