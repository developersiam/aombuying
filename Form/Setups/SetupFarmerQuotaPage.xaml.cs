﻿using AOMBuying.BLL;
using AOMBuying.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AOMBuying.Form.Setups
{
    /// <summary>
    /// Interaction logic for FarmerQuotaPage.xaml
    /// </summary>
    public partial class SetupFarmerQuotaPage : Page
    {
        Crop _crop;
        List<FarmerProject> _farmerProjectList;

        public SetupFarmerQuotaPage()
        {
            try
            {
                InitializeComponent();

                _crop = new Crop();
                _crop = BuyingService.CropBLL().GetDefaultCrop();
                _farmerProjectList = new List<FarmerProject>();
                
                List<ProjectType> _projectTypeList = new List<ProjectType>();
                _projectTypeList = BuyingService.ProjectTypeRepository().GetAll().OrderBy(p => p.ProjectTypeID).ToList();
                
                FarmerIDComboBox.ItemsSource = null;
                FarmerIDComboBox.ItemsSource = BuyingService.FarmerBLL().GetAll().OrderBy(f => f.FarmerID);
                ProjectTypeComboBox.ItemsSource = null;
                ProjectTypeComboBox.ItemsSource = _projectTypeList;
                ProjectTypeFilterComboBox.ItemsSource = null;
                ProjectTypeFilterComboBox.ItemsSource = _projectTypeList;
                ReloadFarmerProjectList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearForm()
        {
            FarmerIDComboBox.SelectedIndex = -1;
            ProjectTypeComboBox.SelectedIndex = -1;
            QuotaTextBox.Text = "";
            SaveButton.Content = "Save";
        }

        private void ReloadFarmerProjectList()
        {
            try
            {
                FarmerProjectDataGrid.ItemsSource = null;
                _farmerProjectList = BuyingService.FarmerProjectBLL().GetByCrop(_crop.Crop1);

                if (ProjectTypeFilterComboBox.SelectedIndex < 0)
                    return;

                _farmerProjectList = _farmerProjectList
                    .Where(fp => fp.ProjectTypeID == ProjectTypeFilterComboBox.SelectedValue.ToString()).ToList();
                FarmerProjectDataGrid.ItemsSource = _farmerProjectList;
                TotalFarmersTextBox.Text = string.Format("{0:N0}", _farmerProjectList.Count());
                TotalQuotaTextBox.Text = string.Format("{0:N0}", _farmerProjectList.Sum(fp => fp.Quota));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (FarmerIDComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("Please select farmer.", "message", MessageBoxButton.OK, MessageBoxImage.Warning);
                    FarmerIDComboBox.Focus();
                    return;
                }
                if (ProjectTypeComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("Please select project.", "message", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ProjectTypeComboBox.Focus();
                    return;
                }
                if (QuotaTextBox.Text == "")
                {
                    MessageBox.Show("Please input quota.", "message", MessageBoxButton.OK, MessageBoxImage.Warning);
                    QuotaTextBox.Focus();
                    return;
                }

                if (SaveButton.Content.ToString() == "Save")
                {
                    BuyingService.FarmerProjectBLL().Add(new FarmerProject
                    {
                        FarmerID = FarmerIDComboBox.SelectedValue.ToString(),
                        ProjectTypeID = ProjectTypeComboBox.SelectedValue.ToString(),
                        Quota = Convert.ToInt32(QuotaTextBox.Text.Replace(" ", string.Empty).Replace(",", string.Empty)),
                        ModifiedDate = DateTime.Now,
                        ModifiedUser = sys_config.CurrentUser,
                        Crop = _crop.Crop1
                    });
                }
                else
                {
                    if (MessageBox.Show("Do you want to edit this item?", "warning!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                        return;
                    BuyingService.FarmerProjectBLL().Update(new FarmerProject
                    {
                        FarmerID = FarmerIDComboBox.SelectedValue.ToString(),
                        ProjectTypeID = ProjectTypeComboBox.SelectedValue.ToString(),
                        Quota = Convert.ToInt32(QuotaTextBox.Text.Replace(" ", string.Empty).Replace(",", string.Empty)),
                        ModifiedDate = DateTime.Now,
                        ModifiedUser = sys_config.CurrentUser,
                        Crop = _crop.Crop1
                    });
                }

                ReloadFarmerProjectList();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (FarmerProjectDataGrid.SelectedIndex < 0)
                    return;
                if (MessageBox.Show("Do you want to delete this item?", "warning!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                // Delete seleted item.
                var seletedItem = (FarmerProject)FarmerProjectDataGrid.SelectedItem;
                BuyingService.FarmerProjectBLL().Delete(seletedItem);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (FarmerProjectDataGrid.SelectedIndex < 0)
                    return;

                var seletedItem = (FarmerProject)FarmerProjectDataGrid.SelectedItem;
                FarmerIDComboBox.SelectedValue = seletedItem.FarmerID;
                ProjectTypeComboBox.SelectedValue = seletedItem.ProjectTypeID;
                QuotaTextBox.Text = string.Format("{0:N0}", seletedItem.Quota);
                SaveButton.Content = "Edit";
                FarmerIDComboBox.IsEditable = false;
                ProjectTypeComboBox.IsEditable = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ProjectTypeFilterComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ReloadFarmerProjectList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
