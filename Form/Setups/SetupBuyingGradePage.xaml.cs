﻿using AOMBuying.BLL;
using AOMBuying.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AOMBuying.Form.Setups
{
    /// <summary>
    /// Interaction logic for SetupBuyingGradePage.xaml
    /// </summary>
    public partial class SetupBuyingGradePage : Page
    {
        BuyingGrade _buyingGrade;
        List<BuyingGrade> _buyingGradeList;
        Crop _crop;
        List<Quality> _qualityList;

        struct Quality
        {
            public string QualityName { get; set; }
        }

        public SetupBuyingGradePage()
        {
            try
            {
                InitializeComponent();

                _buyingGrade = new BuyingGrade();
                _buyingGradeList = new List<BuyingGrade>();
                _crop = new Crop();
                _crop = BuyingService.CropBLL().GetDefaultCrop();
                _qualityList = new List<Quality>();
                _qualityList.Add(new Quality { QualityName = "H" });
                _qualityList.Add(new Quality { QualityName = "M" });
                _qualityList.Add(new Quality { QualityName = "L" });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                _buyingGradeList = BuyingService.BuyingGradeBLL()
                    .GetBuyingGradeByCrop(_crop.Crop1);

                List<ProjectType> _projectTypes = new List<ProjectType>();
                _projectTypes = BuyingService.ProjectTypeRepository().GetAll().ToList();

                TypeComboBox.ItemsSource = null;
                TypeComboBox.ItemsSource = _projectTypes;

                TypeFilterComboBox.ItemsSource = null;
                TypeFilterComboBox.ItemsSource = _projectTypes;

                QualityComboBox.ItemsSource = null;
                QualityComboBox.ItemsSource = _qualityList;

                List<Crop> _cropList = new List<Crop>();
                _cropList = BuyingService.CropBLL().GetAllCrops().OrderBy(c => c.Crop1).ToList();

                CropComboBox.ItemsSource = null;
                CropComboBox.ItemsSource = _cropList;

                CropFilterComboBox.ItemsSource = null;
                CropFilterComboBox.ItemsSource = _cropList;

                ReloadBuyingGradeList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void ReloadBuyingGradeList()
        {
            try
            {
                if (TypeFilterComboBox.SelectedIndex < 0)
                    return;

                if (CropFilterComboBox.SelectedIndex < 0)
                    return;

                _buyingGradeList = BuyingService.BuyingGradeBLL()
                    .GetBuyingGradeByCrop(_crop.Crop1);

                BuyingGradeDataGrid.ItemsSource = null;
                BuyingGradeDataGrid.ItemsSource = _buyingGradeList
                    .Where(b => b.ProjectTypeID == TypeFilterComboBox.SelectedValue.ToString()
                    && b.BuyingGradeCrop == Convert.ToInt16(CropFilterComboBox.SelectedValue))
                    .OrderBy(b => b.BuyingGrade1);

                TotalBuyingGradeTextBox.Text = string.Format("{0:N0}", BuyingGradeDataGrid.Items.Count);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearForm()
        {
            CropComboBox.SelectedIndex = 0;
            TypeComboBox.SelectedIndex = 0;
            QualityComboBox.SelectedIndex = 0;
            BuyingGradeTextBox.Text = "";
            UnitPriceTextBox.Text = "";
            SaveButton.Content = "Save";
            CropComboBox.IsEnabled = true;
            TypeComboBox.IsEnabled = true;

            ReloadBuyingGradeList();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (TypeFilterComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("Please select a tobacco type.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (CropFilterComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("Please select a crop.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if(QualityComboBox.SelectedIndex <=-1)
                {
                    MessageBox.Show("Please select a quality.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (BuyingGradeTextBox.Text == "")
                {
                    MessageBox.Show("Please input a buying grade.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (UnitPriceTextBox.Text == "")
                {
                    MessageBox.Show("Please input a unit price.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (SaveButton.Content.ToString() == "Save")
                {
                    BuyingService.BuyingGradeBLL().AddBuyingGrade(new BuyingGrade
                    {
                        BuyingGrade1 = BuyingGradeTextBox.Text,
                        BuyingGradeCrop = Convert.ToInt16(CropComboBox.SelectedValue),
                        ProjectTypeID = TypeComboBox.SelectedValue.ToString(),
                        Quality = QualityComboBox.SelectedValue.ToString().Replace(" ", string.Empty),
                        UnitPrice = Convert.ToDecimal(UnitPriceTextBox.Text.Replace(" ",string.Empty).Replace(",",string.Empty)),
                        ModifiedDate = DateTime.Now,
                        ModifiedUser = sys_config.CurrentUser
                    });
                }
                else
                {
                    if (MessageBox.Show("Do you want to edit this item?", "warning!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                        return;

                    BuyingService.BuyingGradeBLL().UpdateBuyingGrade(new BuyingGrade
                    {
                        BuyingGrade1 = BuyingGradeTextBox.Text,
                        BuyingGradeCrop = Convert.ToInt16(CropComboBox.SelectedValue),
                        ProjectTypeID = TypeComboBox.SelectedValue.ToString(),
                        Quality = QualityComboBox.SelectedValue.ToString().Replace(" ", string.Empty),
                        UnitPrice = Convert.ToDecimal(UnitPriceTextBox.Text.Replace(" ", string.Empty).Replace(",", string.Empty)),
                        ModifiedDate = DateTime.Now,
                        ModifiedUser = sys_config.CurrentUser
                    });
                }
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BuyingGradeDataGrid.SelectedIndex < 0)
                    return;

                var selectedItem = (BuyingGrade)BuyingGradeDataGrid.SelectedItem;
                CropComboBox.SelectedValue = selectedItem.BuyingGradeCrop;
                TypeComboBox.SelectedValue = selectedItem.ProjectTypeID;
                QualityComboBox.SelectedValue = selectedItem.Quality;
                BuyingGradeTextBox.Text = selectedItem.BuyingGrade1;
                UnitPriceTextBox.Text = string.Format("{0:N0}", selectedItem.UnitPrice);
                SaveButton.Content = "Edit";
                CropComboBox.IsEnabled = false;
                TypeComboBox.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BuyingGradeDataGrid.SelectedIndex < 0)
                    return;

                if (MessageBox.Show("Do you want to delete this item?", "warning!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                var selectedItem = (BuyingGrade)BuyingGradeDataGrid.SelectedItem;
                BuyingService.BuyingGradeBLL().DeleteBuyingGrade(selectedItem);
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CropFilterComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ReloadBuyingGradeList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void TypeFilterComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ReloadBuyingGradeList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
