﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AOMBuying.DomainModel;
using AOMBuying.BLL;
using AOMBuying.ViewModel;
using AOMBuying.Utility;

namespace AOMBuying.Form.Transportations
{
    /// <summary>
    /// Interaction logic for TransportationDocumentPage.xaml
    /// </summary>
    public partial class TransportationDocuments : Window
    {
        List<vm_TransportationDocument> _transportationDocumentList;

        public TransportationDocuments()
        {
            try
            {
                InitializeComponent();

                _transportationDocumentList = new List<vm_TransportationDocument>();
                createDocumentDatePicker.SelectedDate = DateTime.Now;
                ReloadDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReloadDataGrid()
        {
            try
            {
                if (createDocumentDatePicker.SelectedDate == null)
                    return;

                _transportationDocumentList = Helper.TransportationHelper.
                    GetByCreateDate(Convert.ToDateTime(createDocumentDatePicker.SelectedDate));

                transportationDataGrid.ItemsSource = null;
                transportationDataGrid.ItemsSource = _transportationDocumentList
                    .OrderByDescending(t => t.CreateDate)
                    .ToList();

                totalItemLabel.Content = string.Format("{0:N0}", _transportationDocumentList.Count());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CreateDocument()
        {
            try
            {
                if (createDocumentDatePicker.SelectedDate == null)
                {
                    MessageBox.Show("Please select a create document date.",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    createDocumentDatePicker.Focus();
                    return;
                }

                if (truckNumberTextBox.Text == "")
                {
                    MessageBox.Show("Please input a truck number.", "warning",
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                    truckNumberTextBox.Focus();
                    return;
                }

                BuyingService.TransportationBLL()
                    .Add(sys_config.DefaultCrop.Crop1,
                    truckNumberTextBox.Text.Replace(" ", string.Empty),
                    Convert.ToDateTime(createDocumentDatePicker.SelectedDate),
                    sys_config.CurrentUser);

                ReloadDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void createDocumentButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CreateDocument();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ReloadDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ReloadDataGrid();
                truckNumberTextBox.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void truckNumberTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    CreateDocument();
                    truckNumberTextBox.Text = "";
                    truckNumberTextBox.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (transportationDataGrid.SelectedIndex < 0)
                    return;

                if (MessageBox.Show("Do you want to delete this item?", "Warning!", 
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                var item = (vm_TransportationDocument)transportationDataGrid.SelectedItem;

                // Delete item.
                BuyingService.TransportationBLL().Delete(item.TransportationCode);

                ReloadDataGrid();
                truckNumberTextBox.Text = "";
                truckNumberTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void viewDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (transportationDataGrid.SelectedIndex < 0)
                    return;

                var item = (vm_TransportationDocument)transportationDataGrid.SelectedItem;

                TransportationDetail window = new TransportationDetail(item.TransportationCode);
                window.ShowDialog();
                ReloadDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void createDocumentDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ReloadDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
