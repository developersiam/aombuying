﻿using AOMBuying.BLL;
using AOMBuying.DomainModel;
using AOMBuying.Utility;
using AOMBuying.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AOMBuying.Form.Transportations
{
    /// <summary>
    /// Interaction logic for TransportationDetailWindow.xaml
    /// </summary>
    public partial class TransportationDetail : Window
    {
        Buying _buying;
        string _transportationCode;
        vm_TransportationDocument _document;

        public TransportationDetail(string transportationCode)
        {
            try
            {
                InitializeComponent();

                _buying = new Buying();
                _document = new vm_TransportationDocument();

                _transportationCode = transportationCode;
                loadingRadioButton.IsChecked = true;
                ReloadDocument();
                ReloadDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReloadDocument()
        {
            try
            {
                _document = Helper.TransportationHelper.GetSingle(_transportationCode);
                transportationCodeTextBox.Text = _document.TransportationCode;
                truckNumberTextBox.Text = _document.TransportationDocument.TruckNo;
                lockedStatusButton.Visibility = _document.TransportationDocument.IsFinish == true ? Visibility.Collapsed : Visibility.Visible;
                unlockedStatusButton.Visibility = _document.TransportationDocument.IsFinish == true ? Visibility.Visible : Visibility.Collapsed;
                printBuyingVoucherButton.IsEnabled = _document.TransportationDocument.IsFinish;
                exportToExcelButton.IsEnabled = _document.TransportationDocument.IsFinish;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReloadDataGrid()
        {
            try
            {
                var list = BuyingService.BuyingBLL().GetByTransportationDocument(_transportationCode);                   

                buyingDataGrid.ItemsSource = null;
                buyingDataGrid.ItemsSource = list.OrderByDescending(x => x.LoadBaleToTruckDate);

                totalBaleTextBox.Text = string.Format("{0:N0}", list.Count());
                totalWeightTextBox.Text = string.Format("{0:N1}", list.Sum(b => b.BuyingWeight));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void PrintBuyingVoucherButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Report.RPTBUY02 window = new Report.RPTBUY02(_document);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ExportToExcelButton_Click(object sender, RoutedEventArgs e)
        {
            try
            { 
                System.Windows.Forms.FolderBrowserDialog folderDlg = new System.Windows.Forms.FolderBrowserDialog();
                folderDlg.ShowNewFolderButton = true;

                // Show the FolderBrowserDialog.
                System.Windows.Forms.DialogResult result = folderDlg.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    string folderPath = Helper.TransportationHelper.ExportTransportationDocument(_document.TransportationCode, folderDlg.SelectedPath);
                    Environment.SpecialFolder root = folderDlg.RootFolder;
                    MessageBox.Show("Export complete.", "Complete", MessageBoxButton.OK, MessageBoxImage.Information);
                    System.Diagnostics.Process.Start(folderPath);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UnlockedStatusButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BuyingService.TransportationBLL().UnFinish(_document.TransportationDocument.TransportationCode);
                ReloadDocument();
                ReloadDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void LockedStatusButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BuyingService.BuyingBLL().GetByTransportationDocument(_transportationCode).Count < 1)
                    if (MessageBox.Show("For all the truck should be have at least 1 bale., Do you want to finish this truck?", "warning", 
                        MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                        return;

                if (MessageBox.Show("Do you want to finish this truck?", "warning",
                        MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BuyingService.TransportationBLL().Finish(_document.TransportationDocument.TransportationCode);

                ReloadDocument();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BaleBarcodeTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                if (baleBarcodeTextBox.Text == "")
                {
                    MessageBox.Show("Please input a bale barcode.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Focus();
                    baleBarcodeTextBox.Clear();
                    return;
                }

                _buying = BuyingService.BuyingBLL().GetByBaleBarcode(baleBarcodeTextBox.Text.Replace(" ", ""));

                if (_buying == null)
                {
                    MessageBox.Show("Find a data not found!", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Focus();
                    baleBarcodeTextBox.Clear();
                    baleNumberTextBox.Clear();
                    return;
                }

                baleNumberTextBox.Text = string.Format("{0:N0}", _buying.BaleNumber);
                Save(_buying.BaleBarcode);
                baleBarcodeTextBox.Clear();
                baleNumberTextBox.Clear();
                baleBarcodeTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BaleNumberTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                if (baleNumberTextBox.Text == "")
                {
                    MessageBox.Show("Please input a bale number.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleNumberTextBox.Focus();
                    baleNumberTextBox.Clear();
                    return;
                }

                _buying = BuyingService.BuyingBLL()
                    .GetByBaleNumber(Convert.ToInt32(baleNumberTextBox.Text.Replace(" ", string.Empty)));

                if (_buying == null)
                {
                    MessageBox.Show("Find a data not found!", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Focus();
                    baleNumberTextBox.Clear();
                    baleBarcodeTextBox.Clear();
                    return;
                }

                baleBarcodeTextBox.Text = _buying.BaleBarcode;
                Save(_buying.BaleBarcode);
                baleNumberTextBox.Clear();
                baleBarcodeTextBox.Clear();
                baleNumberTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void TakeOffRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            loadingRadioButton.IsChecked = false;
        }

        private void Save(string baleBarcode)
        {
            try
            {
                if (baleBarcode == "")
                {
                    MessageBox.Show("Please input a bale barcode.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Focus();
                    return;
                }

                if (loadingRadioButton.IsChecked == true)
                {
                    if (_buying.TransportationCode != null 
                        && _buying.TransportationCode == _document.TransportationCode)
                        if (MessageBox.Show("This bale was loaded to this truck already. Do you want to replace by a new loading date?"
                            , "warning!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                            return;

                    if (_buying.TransportationCode != null
                        && _buying.TransportationCode != _document.TransportationCode
                        && _buying.TransportationDocument.IsFinish == true)
                    {
                        MessageBox.Show("The transportation document of this bale was set to finished already." + Environment.NewLine
                            + "You cannot move this bale to this truck." + Environment.NewLine
                            + "Please goto transport code " + _buying.TransportationCode 
                            + " and click unlock before move the bale.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }

                    if (_buying.TransportationCode != null && _buying.TransportationCode != _document.TransportationCode)
                        if (MessageBox.Show("This bale was loaded to another truck already (" +
                            _buying.TransportationCode + "). Do you want to move this bale to this truck?"
                            , "warning!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                            return;

                    BuyingService.TransportationBLL().BringUp(baleBarcode, sys_config.CurrentUser, _document.TransportationCode);
                }
                else
                {
                    if (_buying.TransportationCode != null && _buying.TransportationCode != _document.TransportationCode)
                    {
                        MessageBox.Show("This bale difference a transportation document. " +
                            "You cannot remove this bale from this transportation document.", "warning!", 
                            MessageBoxButton.OK, MessageBoxImage.Question);
                        return;
                    }
                    BuyingService.TransportationBLL().BringDown(baleBarcode);
                }

                ReloadDocument();
                ReloadDataGrid();
                baleBarcodeTextBox.Text = "";
                baleNumberTextBox.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
