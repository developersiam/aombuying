﻿using AOMBuying.BLL;
using AOMBuying.DomainModel;
using AOMBuying.ViewModel;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOMBuying.Helper
{
    public static class TransportationHelper
    {
        public static vm_TransportationDocument GetSingle(string transportationCode)
        {
            var model = BuyingService.TransportationBLL().GetSingle(transportationCode);

            return new vm_TransportationDocument
            {
                TransportationCode = model.TransportationCode,
                TransportationDocument = model,
                TotalBales = model.Buyings.Count,
                TotalWeight = Convert.ToDouble(model.Buyings.Sum(b => b.BuyingWeight))
            };
        }
        public static List<vm_TransportationDocument> GetByCreateDate(DateTime createDate)
        {
            List<TransportationDocument> transportationDocumentList = new List<TransportationDocument>();
            transportationDocumentList = BuyingService.TransportationBLL().GetByCreateDate(createDate).ToList();

            return (from t in transportationDocumentList
                    select new vm_TransportationDocument
                    {
                        TransportationCode = t.TransportationCode,
                        TransportationDocument = t,
                        TotalBales = t.Buyings.Count,
                        TotalWeight = Convert.ToDouble(t.Buyings.Sum(b => b.BuyingWeight))
                    }).ToList();
        }
        public static List<vm_TransportationDocument> GetByCrop(short crop)
        {
            List<TransportationDocument> transportationDocumentList = new List<TransportationDocument>();
            transportationDocumentList = BuyingService.TransportationBLL().GetByCrop(crop).ToList();

            return (from t in transportationDocumentList
                    select new vm_TransportationDocument
                    {
                        TransportationCode = t.TransportationCode,
                        TransportationDocument = t,
                        TotalBales = t.Buyings.Count,
                        TotalWeight = Convert.ToDouble(t.Buyings.Sum(b => b.BuyingWeight))
                    }).ToList();
        }
        public static string ExportTransportationDocument(string transportationCode, string folderName)
        {
            var vm_document = GetSingle(transportationCode);

            List<vm_Buying> list = new List<vm_Buying>();
            list = BuyingHelper.GetByTransportationDocument(transportationCode);

            string name = @"" + folderName + "\\" + vm_document.TransportationDocument.TransportationCode + ".xlsx";
            FileInfo info = new FileInfo(name);

            if (info.Exists)
                File.Delete(name);

            using (ExcelPackage pck = new ExcelPackage(info))
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add(vm_document.TransportationDocument.TransportationCode);
                ws.Cells["A1"].LoadFromCollection(list, true);
                ws.Protection.IsProtected = false;
                using (ExcelRange col = ws.Cells[2, 6, 2 + list.Count, 6]) { col.Style.Numberformat.Format = "dd/MM/yyyy HH:mm"; }
                using (ExcelRange col = ws.Cells[2, 14, 2 + list.Count, 14]) { col.Style.Numberformat.Format = "dd/MM/yyyy HH:mm"; }
                using (ExcelRange col = ws.Cells[2, 19, 2 + list.Count, 19]) { col.Style.Numberformat.Format = "dd/MM/yyyy HH:mm"; }
                using (ExcelRange col = ws.Cells[2, 22, 2 + list.Count, 22]) { col.Style.Numberformat.Format = "dd/MM/yyyy HH:mm"; }
                using (ExcelRange col = ws.Cells[2, 24, 2 + list.Count, 24]) { col.Style.Numberformat.Format = "dd/MM/yyyy HH:mm"; }
                pck.Save();
            }

            return @"" + folderName;
        }
    }
}
