﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AOMBuying.Helper
{
    public static class RegularExpressionHelper
    {
        public static bool IsDecimalCharacter(string str)
        {
            Regex rgx = new Regex(@"^\d+(.\d+){0,1}$");
            if (!rgx.IsMatch(str))
                return false;
            else
                return true;
        }
        public static bool IsNumericCharacter(string str)
        {
            Regex rgx = new Regex(@"^([0-9]+)$");
            if (!rgx.IsMatch(str))
                return false;
            else
                return true;
        }
        public static bool IsNumericAndCharacter(string str)
        {
            Regex rgx = new Regex(@"^([a-zA-Z0-9/]+)$");
            if (!rgx.IsMatch(str))
                return false;
            else
                return true;
        }
    }
}
