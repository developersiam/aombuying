﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOMBuying.ViewModel;
using AOMBuying.BLL;
using AOMBuying.DomainModel;

namespace AOMBuying.Helper
{
    public static class FarmerQuotaHelper
    {
        public static List<vm_FarmerCropQuota> GetByCropAndProjectType(short crop, string projectTypeID)
        {
            //Summary farmer quota by crop.
            List<FarmerProject> list = new List<FarmerProject>();
            list = BuyingService.FarmerProjectBLL().GetByCropAndProject(crop, projectTypeID).ToList();

            //Summary farmer sold by project type.
            List<vm_FarmerCropQuota> list2 = new List<vm_FarmerCropQuota>();
            list2 = BuyingService.BuyingBLL().GetByCropAndProjectType(crop, projectTypeID).
                        Where(b => b.BuyingWeight != null).
                        GroupBy(b => new { b.Crop, b.FarmerID, b.ProjectTypeID }).
                        Select(r => new vm_FarmerCropQuota
                        {
                            Crop = r.First().Crop,
                            FarmerID = r.First().FarmerID,
                            ProjectTypeID = r.First().ProjectTypeID,
                            TotalBale = r.Count(),
                            KgSold = Convert.ToDouble(r.Sum(b => b.BuyingWeight)),
                            TotalPrice = r.Sum(b => b.BuyingWeight) == 0 ? 0 : Convert.ToDouble(r.Sum(b => b.BuyingWeight * (b.BuyingGrade == null ? 0 : b.BuyingGrade1.UnitPrice)))
                        }).ToList();

            //Return summary quota and accumulative sold.
            return (from a in list
                    join b in list2
                    on new { a.Crop, a.FarmerID, a.ProjectTypeID } equals new { b.Crop, b.FarmerID, b.ProjectTypeID } into gj
                    from subBuying in gj.DefaultIfEmpty()
                    select new vm_FarmerCropQuota
                    {
                        Crop = a.Crop,
                        FarmerID = a.FarmerID,
                        ProjectTypeID = a.ProjectTypeID,
                        Quota = a.Quota,
                        FarmerProject = a,
                        TotalBale = subBuying == null ? 0 : subBuying.TotalBale,
                        KgSold = subBuying == null ? 0 : subBuying.KgSold,
                        KgBalance = subBuying == null ? a.Quota : a.Quota - subBuying.KgSold,
                        TotalPrice = subBuying == null ? 0 : subBuying.TotalPrice
                    }).ToList();
        }
        public static List<vm_FarmerCropQuota> GetByFarmer(short crop, string farmerID)
        {
            return (from a in BuyingService.FarmerProjectBLL().GetByFarmer(crop, farmerID)
                    join b in
                        BuyingService.BuyingBLL().GetByFarmer(crop, farmerID).
                        GroupBy(b => b.ProjectTypeID).
                        Select(r => new vm_FarmerCropQuota
                        {
                            Crop = r.First().Crop,
                            FarmerID = r.First().FarmerID,
                            ProjectTypeID = r.First().ProjectTypeID,
                            KgSold = Convert.ToDouble(r.Sum(b => b.BuyingWeight)),
                            TotalPrice = r.Count() <= 0 ? 0 : Convert.ToDouble(r.Sum(b => b.BuyingWeight * (b.BuyingGrade == null ? 0 : b.BuyingGrade1.UnitPrice))),
                            TotalBale = r.Count()
                        })
                    on a.ProjectTypeID equals b.ProjectTypeID into gj
                    from subBuying in gj.DefaultIfEmpty()
                    select new vm_FarmerCropQuota
                    {
                        Crop = a.Crop,
                        FarmerID = a.FarmerID,
                        ProjectTypeID = a.ProjectTypeID,
                        Quota = a.Quota,
                        FarmerProject = a,
                        TotalBale = subBuying == null ? 0 : subBuying.TotalBale,
                        KgSold = subBuying == null ? 0 : subBuying.KgSold,
                        KgBalance = subBuying == null ? a.Quota : a.Quota - subBuying.KgSold,
                        TotalPrice = subBuying == null ? 0 : subBuying.TotalPrice
                    }).ToList();
        }
        public static List<vm_FarmerCropQuota> GetByProject(short crop, string projectTypeID)
        {
            List<BuyingGrade> buyingGradeList = new List<BuyingGrade>();
            buyingGradeList = BuyingService.BuyingGradeBLL().GetBuyingGradeByCrop(crop).Where(bg => bg.ProjectTypeID == projectTypeID).ToList();
            return (from a in BuyingService.FarmerProjectBLL().GetByCropAndProject(crop, projectTypeID)
                    join b in
                        BuyingService.BuyingBLL().GetByCrop(crop).
                        Where(b => b.ProjectTypeID == projectTypeID && b.BuyingWeight != null).
                        GroupBy(b => new { b.Crop, b.ProjectTypeID, b.FarmerID }).
                        Select(r => new vm_FarmerCropQuota
                        {
                            Crop = r.First().Crop,
                            FarmerID = r.First().FarmerID,
                            ProjectTypeID = r.First().ProjectTypeID,
                            KgSold = Convert.ToDouble(r.Sum(b => b.BuyingWeight)),
                            TotalPrice = Convert.ToDouble(r.Sum(b => b.BuyingWeight * (b.BuyingGrade == null ? 0 : b.BuyingGrade1.UnitPrice))),
                            TotalBale = r.Where(b => b.BuyingGrade != null).Count()
                        })
                    on new { Crop = a.Crop, FarmerID = a.FarmerID, ProjectTypeID = a.ProjectTypeID }
                    equals new { Crop = b.Crop, FarmerID = b.FarmerID, ProjectTypeID = b.ProjectTypeID } into gj
                    from subBuying in gj.DefaultIfEmpty()
                    select new vm_FarmerCropQuota
                    {
                        Crop = a.Crop,
                        FarmerID = a.FarmerID,
                        ProjectTypeID = a.ProjectTypeID,
                        Quota = a.Quota,
                        FarmerProject = a,
                        TotalBale = subBuying == null ? 0 : subBuying.TotalBale,
                        KgSold = subBuying == null ? 0 : subBuying.KgSold,
                        KgBalance = subBuying == null ? a.Quota : a.Quota - subBuying.KgSold,
                        TotalPrice = subBuying == null ? 0 : subBuying.TotalPrice
                    }).ToList();
        }
    }
}
