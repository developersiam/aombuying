﻿using AOMBuying.BLL;
using AOMBuying.DomainModel;
using AOMBuying.ViewModel;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOMBuying.Helper
{
    public static class BuyingHelper
    {
        public static List<vm_Buying> GetByBuyingDocument(vm_BuyingDocument buyingDocumentViewModel)
        {
            DateTime? nullDateReturn = null;

            BuyingDocument _buyingDocument = new BuyingDocument();
            _buyingDocument = BuyingService.BuyingDocumentBLL().GetSingle(buyingDocumentViewModel.Crop,
                buyingDocumentViewModel.FarmerID,
                buyingDocumentViewModel.DocumentNumber);

            return (from b in BuyingService.BuyingBLL().GetByDocument(_buyingDocument)
                    select new vm_Buying
                    {
                        BaleBarcode = b.BaleBarcode,
                        Crop = b.Crop,
                        FarmerID = b.FarmerID,
                        DocumentNumber = b.DocumentNumber,
                        BuyingDocumentCode = "CY-" + b.Crop + "-" + b.FarmerID + "-" + b.DocumentNumber,
                        ProjectTypeID = b.ProjectTypeID,
                        RegisterBarcodeDate = b.RegisterBarcodeDate,
                        RegisterBarcodeUser = b.RegisterBarcodeUser,
                        RejectTypeID = b.RejectTypeID,
                        BuyingGradeCrop = b.BuyingGradeCrop,
                        BuyingGrade = b.BuyingGrade,
                        UnitPrice = b.BuyingGrade == null ? 0 : b.BuyingGrade1.UnitPrice,
                        Quality = b.BuyingGrade == null ? null : b.BuyingGrade1.Quality,
                        BuyingGradeRecordDate = b.BuyingGradeRecordDate,
                        BuyingGradeRecordUser = b.BuyingGradeRecordUser,
                        BuyingWeight = b.BuyingWeight,
                        BuyingWeightRecordDate = b.BuyingWeightRecordDate,
                        BuyingWeightRecordUser = b.BuyingWeightRecordUser,
                        TransportationCode = b.TransportationCode,
                        LoadBaleToTruckDate = b.LoadBaleToTruckDate,
                        LoadBaleToTruckUser = b.LoadBaleToTruckUser,
                        BaleNumber = b.BaleNumber,
                        Price = b.BuyingGrade == null ? 0 : Convert.ToDecimal(b.BuyingGrade1.UnitPrice * b.BuyingWeight),
                        TruckNo = b.TransportationCode == null ? null : b.TransportationDocument.TruckNo,
                        CreateTransportationDate = b.TransportationCode == null ? nullDateReturn : b.TransportationDocument.CreateDate,
                        RejectTypeName = b.RejectTypeID == null ? "" : b.RejectType.RejectTypeName
                    }).ToList();
        }
        public static List<vm_Buying> GetByTransportationDocument(string transportationCode)
        {
            DateTime? nullDateReturn = null;

            return (from b in BuyingService.BuyingBLL().GetByTransportationDocument(transportationCode)
                    select new vm_Buying
                    {
                        BaleBarcode = b.BaleBarcode,
                        Crop = b.Crop,
                        FarmerID = b.FarmerID,
                        DocumentNumber = b.DocumentNumber,
                        BuyingDocumentCode = "CY-" + b.Crop + "-" + b.FarmerID + "-" + b.DocumentNumber,
                        ProjectTypeID = b.ProjectTypeID,
                        RegisterBarcodeDate = b.RegisterBarcodeDate,
                        RegisterBarcodeUser = b.RegisterBarcodeUser,
                        RejectTypeID = b.RejectTypeID,
                        BuyingGradeCrop = b.BuyingGradeCrop,
                        BuyingGrade = b.BuyingGrade,
                        UnitPrice = b.BuyingGrade == null ? 0 : b.BuyingGrade1.UnitPrice,
                        Quality = b.BuyingGrade == null ? null : b.BuyingGrade1.Quality,
                        BuyingGradeRecordDate = b.BuyingGradeRecordDate,
                        BuyingGradeRecordUser = b.BuyingGradeRecordUser,
                        BuyingWeight = b.BuyingWeight,
                        BuyingWeightRecordDate = b.BuyingWeightRecordDate,
                        BuyingWeightRecordUser = b.BuyingWeightRecordUser,
                        TransportationCode = b.TransportationCode,
                        LoadBaleToTruckDate = b.LoadBaleToTruckDate,
                        LoadBaleToTruckUser = b.LoadBaleToTruckUser,
                        BaleNumber = b.BaleNumber,
                        Price = b.BuyingGrade == null ? 0 : Convert.ToDecimal(b.BuyingGrade1.UnitPrice * b.BuyingWeight),
                        TruckNo = b.TransportationCode == null ? null : b.TransportationDocument.TruckNo,
                        CreateTransportationDate = b.TransportationCode == null ? nullDateReturn : b.TransportationDocument.CreateDate,
                        RejectTypeName = b.RejectTypeID == null ? null : b.RejectType.RejectTypeName
                    }).ToList();
        }
        public static List<vm_Buying> GetByCrop(short crop)
        {
            DateTime? nullDateReturn = null;
            List<Buying> list = new List<Buying>();
            list = BuyingService.BuyingBLL().GetByCrop(crop).ToList();

            return (from b in list
                    select new vm_Buying
                    {
                        BaleBarcode = b.BaleBarcode,
                        Crop = b.Crop,
                        FarmerID = b.FarmerID,
                        DocumentNumber = b.DocumentNumber,
                        BuyingDocumentCode = "CY-" + b.Crop + "-" + b.FarmerID + "-" + b.DocumentNumber,
                        ProjectTypeID = b.ProjectTypeID,
                        RegisterBarcodeDate = b.RegisterBarcodeDate,
                        RegisterBarcodeUser = b.RegisterBarcodeUser,
                        RejectTypeID = b.RejectTypeID,
                        BuyingGradeCrop = b.BuyingGradeCrop,
                        BuyingGrade = b.BuyingGrade,
                        UnitPrice = b.BuyingGrade == null ? 0 : b.BuyingGrade1.UnitPrice,
                        Quality = b.BuyingGrade == null ? null : b.BuyingGrade1.Quality,
                        BuyingGradeRecordDate = b.BuyingGradeRecordDate,
                        BuyingGradeRecordUser = b.BuyingGradeRecordUser,
                        BuyingWeight = b.BuyingWeight,
                        BuyingWeightRecordDate = b.BuyingWeightRecordDate,
                        BuyingWeightRecordUser = b.BuyingWeightRecordUser,
                        TransportationCode = b.TransportationCode,
                        LoadBaleToTruckDate = b.LoadBaleToTruckDate,
                        LoadBaleToTruckUser = b.LoadBaleToTruckUser,
                        BaleNumber = b.BaleNumber,
                        Price = b.BuyingGrade == null ? 0 : Convert.ToDecimal(b.BuyingGrade1.UnitPrice * b.BuyingWeight),
                        TruckNo = b.TransportationCode == null ? null : b.TransportationDocument.TruckNo,
                        CreateTransportationDate = b.TransportationCode == null ? nullDateReturn : b.TransportationDocument.CreateDate,
                        RejectTypeName = b.RejectTypeID == null ? "" : b.RejectType.RejectTypeName
                    }).ToList();
        }
        public static List<vm_Buying> GetByBuyingDate(DateTime createDocumentDate)
        {
            DateTime? nullDateReturn = null;

            return (from b in BuyingService.BuyingBLL()
                    .GetByBuyingDate(createDocumentDate)
                    select new vm_Buying
                    {
                        BaleBarcode = b.BaleBarcode,
                        Crop = b.Crop,
                        FarmerID = b.FarmerID,
                        DocumentNumber = b.DocumentNumber,
                        BuyingDocumentCode = "CY-" + b.Crop + "-" + b.FarmerID + "-" + b.DocumentNumber,
                        ProjectTypeID = b.ProjectTypeID,
                        RegisterBarcodeDate = b.RegisterBarcodeDate,
                        RegisterBarcodeUser = b.RegisterBarcodeUser,
                        RejectTypeID = b.RejectTypeID,
                        BuyingGradeCrop = b.BuyingGradeCrop,
                        BuyingGrade = b.BuyingGrade,
                        UnitPrice = b.BuyingGrade == null ? 0 : b.BuyingGrade1.UnitPrice,
                        Quality = b.BuyingGrade == null ? null : b.BuyingGrade1.Quality,
                        BuyingGradeRecordDate = b.BuyingGradeRecordDate,
                        BuyingGradeRecordUser = b.BuyingGradeRecordUser,
                        BuyingWeight = b.BuyingWeight,
                        BuyingWeightRecordDate = b.BuyingWeightRecordDate,
                        BuyingWeightRecordUser = b.BuyingWeightRecordUser,
                        TransportationCode = b.TransportationCode,
                        LoadBaleToTruckDate = b.LoadBaleToTruckDate,
                        LoadBaleToTruckUser = b.LoadBaleToTruckUser,
                        BaleNumber = b.BaleNumber,
                        Price = b.BuyingGrade == null ? 0 : Convert.ToDecimal(b.BuyingGrade1.UnitPrice * b.BuyingWeight),
                        TruckNo = b.TransportationCode == null ? null : b.TransportationDocument.TruckNo,
                        CreateTransportationDate = b.TransportationCode == null ? nullDateReturn : b.TransportationDocument.CreateDate,
                        RejectTypeName = b.RejectTypeID == null ? "" : b.RejectType.RejectTypeName
                    }).ToList();
        }
        public static string ExportBuyingCropRawData(short crop, string folderName)
        {
            List<vm_Buying> list = new List<vm_Buying>();
            list = BuyingService.BuyingBLL().GetByCrop(crop).Select(x => new vm_Buying
            {

            }).ToList();

            string name = @"" + folderName + "\\SUMMARY-BUYING-CY" + crop + "-" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".xlsx";
            FileInfo info = new FileInfo(name);

            if (info.Exists)
                File.Delete(name);

            using (ExcelPackage pck = new ExcelPackage(info))
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("BUY-CY" + crop + " " + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day);

                ws.Cells["A1"].LoadFromCollection(list, true);
                ws.Protection.IsProtected = false;

                using (ExcelRange col = ws.Cells[2, 6, 2 + list.Count, 6]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }
                using (ExcelRange col = ws.Cells[2, 11, 2 + list.Count, 11]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }
                using (ExcelRange col = ws.Cells[2, 13, 2 + list.Count, 13]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }
                using (ExcelRange col = ws.Cells[2, 14, 2 + list.Count, 14]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }
                using (ExcelRange col = ws.Cells[2, 17, 2 + list.Count, 17]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }

                pck.Save();
            }

            return @"" + folderName;
        }
    }
}
