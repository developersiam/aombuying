﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOMBuying.DomainModel;
using AOMBuying.BLL;
using AOMBuying.ViewModel;
using System.Data;
using System.Drawing;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using OfficeOpenXml.Drawing;

namespace AOMBuying.Helper
{
    public static class BuyingDocumentHelper
    {        
        public static vm_BuyingDocument GetSingle(short crop, string farmerID, short documentNumber)
        {
            BuyingDocument bd = BuyingService.BuyingDocumentBLL().GetSingle(crop, farmerID, documentNumber);

            return new vm_BuyingDocument
            {
                Farmer = bd.FarmerRegistration.Farmer,
                Crop = bd.Crop,
                FarmerID = bd.FarmerID,
                DocumentNumber = bd.DocumentNumber,
                BuyingDocumentCode = "CY-" + bd.Crop + "-" + bd.FarmerID + "-" + bd.DocumentNumber,
                CreateDate = bd.CreateDate,
                CreateUser = bd.CreateUser,
                FinishDate = bd.FinishDate,
                IsFinish = bd.IsFinish,
                ModifiedDate = bd.ModifiedDate,
                ModifiedUser = bd.ModifiedUser,
                BuyingStationID = bd.BuyingStationID,
                BuyingStation = bd.BuyingStation,
                TotalBale = bd.Buyings.Count(),
                TotalBuying = bd.Buyings.Where(x => x.BuyingWeight != null).Count(),
                TotalReject = bd.Buyings.Where(b => b.RejectTypeID != null).Count(),
                TotalLoadTruck = bd.Buyings.Where(x => x.TransportationCode != null).Count(),
                TotalWeight = Convert.ToDecimal(bd.Buyings.Sum(b => b.BuyingWeight)),
                TotalPrice = Convert.ToDecimal(bd.Buyings.Sum(b => b.BuyingWeight * (b.BuyingGrade == null ? 0 : b.BuyingGrade1.UnitPrice)))
            };
        }
        public static List<vm_BuyingDocument> GetByFarmer(short crop, string farmerID)
        {
            return (from bd in BuyingService.BuyingDocumentBLL().GetByFarmer(crop, farmerID)
                    select new vm_BuyingDocument
                    {
                        Farmer = bd.FarmerRegistration.Farmer,
                        Crop = bd.Crop,
                        FarmerID = bd.FarmerID,
                        DocumentNumber = bd.DocumentNumber,
                        BuyingDocumentCode = "CY-" + bd.Crop + "-" + bd.FarmerID + "-" + bd.DocumentNumber,
                        CreateDate = bd.CreateDate,
                        CreateUser = bd.CreateUser,
                        FinishDate = bd.FinishDate,
                        IsFinish = bd.IsFinish,
                        ModifiedDate = bd.ModifiedDate,
                        ModifiedUser = bd.ModifiedUser,
                        BuyingStationID = bd.BuyingStationID,
                        BuyingStation = bd.BuyingStation,
                        TotalBale = bd.Buyings.Count(),
                        TotalBuying = bd.Buyings.Where(x => x.BuyingWeight != null).Count(),
                        TotalReject = bd.Buyings.Where(b => b.RejectTypeID != null).Count(),
                        TotalLoadTruck = bd.Buyings.Where(x => x.TransportationCode != null).Count(),
                        TotalWeight = Convert.ToDecimal(bd.Buyings.Sum(b => b.BuyingWeight)),
                        TotalPrice = Convert.ToDecimal(bd.Buyings.Sum(b => b.BuyingWeight * (b.BuyingGrade == null ? 0 : b.BuyingGrade1.UnitPrice)))
                    }).ToList();
        }
        public static List<vm_BuyingDocument> GetByCrop(short crop)
        {
            return (from bd in BuyingService.BuyingDocumentBLL().GetByCrop(crop)
                    select new vm_BuyingDocument
                    {
                        Farmer = bd.FarmerRegistration.Farmer,
                        Crop = bd.Crop,
                        FarmerID = bd.FarmerID,
                        DocumentNumber = bd.DocumentNumber,
                        BuyingDocumentCode = "CY-" + bd.Crop + "-" + bd.FarmerID + "-" + bd.DocumentNumber,
                        CreateDate = bd.CreateDate,
                        CreateUser = bd.CreateUser,
                        FinishDate = bd.FinishDate,
                        IsFinish = bd.IsFinish,
                        ModifiedDate = bd.ModifiedDate,
                        ModifiedUser = bd.ModifiedUser,
                        BuyingStationID = bd.BuyingStationID,
                        BuyingStation = bd.BuyingStation,
                        TotalBale = bd.Buyings.Count(),
                        TotalBuying = bd.Buyings.Where(x => x.BuyingWeight != null).Count(),
                        TotalReject = bd.Buyings.Where(b => b.RejectTypeID != null).Count(),
                        TotalLoadTruck = bd.Buyings.Where(x => x.TransportationCode != null).Count(),
                        TotalWeight = Convert.ToDecimal(bd.Buyings.Sum(b => b.BuyingWeight)),
                        //TotalPrice = Convert.ToDecimal(bd.Buyings.Sum(b => b.BuyingWeight * (b.BuyingGrade == null ? 0 : b.BuyingGrade1.UnitPrice)))
                    }).ToList();
        }
        public static List<vm_BuyingDocument> GetByCreateDate(DateTime createDate)
        {
            return (from bd in BuyingService.BuyingDocumentBLL().GetByCreateDate(createDate)
                    select new vm_BuyingDocument
                    {
                        Farmer = bd.FarmerRegistration.Farmer,
                        Crop = bd.Crop,
                        FarmerID = bd.FarmerID,
                        DocumentNumber = bd.DocumentNumber,
                        BuyingDocumentCode = "CY-" + bd.Crop + "-" + bd.FarmerID + "-" + bd.DocumentNumber,
                        CreateDate = bd.CreateDate,
                        CreateUser = bd.CreateUser,
                        FinishDate = bd.FinishDate,
                        IsFinish = bd.IsFinish,
                        ModifiedDate = bd.ModifiedDate,
                        ModifiedUser = bd.ModifiedUser,
                        BuyingStationID = bd.BuyingStationID,
                        BuyingStation = bd.BuyingStation,
                        TotalBale = bd.Buyings.Count(),
                        TotalBuying = bd.Buyings.Where(x=>x.BuyingWeight != null).Count(),
                        TotalReject = bd.Buyings.Where(b => b.RejectTypeID != null).Count(),
                        TotalLoadTruck = bd.Buyings.Where(x=>x.TransportationCode != null).Count(),
                        TotalWeight = Convert.ToDecimal(bd.Buyings.Sum(b => b.BuyingWeight)),
                        TotalPrice = Convert.ToDecimal(bd.Buyings.Sum(b => b.BuyingWeight * (b.BuyingGrade == null ? 0 : b.BuyingGrade1.UnitPrice)))
                    }).ToList();
        }
    }
}
