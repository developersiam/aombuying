﻿using AOMBuying.BLL;
using AOMBuying.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOMBuying
{
    public static class sys_config
    {
        public static Crop DefaultCrop
        {
            get
            {
                return BuyingService.CropBLL().GetDefaultCrop();
            }
        }
        public static string CurrentUser
        {
            get
            {
                return Environment.UserDomainName + "\\" + Environment.UserName;
            }
        }
    }
}
